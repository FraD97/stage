package com.sini.layout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.ImageView;
import android.widget.TextView;

import com.sini.layout.Activities.ConversationActivity;

public class MainActivity extends AppCompatActivity {

    private TextView textViewTouchMe;
    private CardView cardViewConversation;
    private CardView cardViewServices;
    private ImageView imageViewExit;
    private View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Intent intent = null;
            switch(view.getId()){
                case R.id.cardviewConversation:
                    print("conversation");
                    intent = new Intent(getApplicationContext(), ConversationActivity.class);
                    startActivity(intent);
                    finish();
                    break;
                case R.id.cardviewService:
                    intent = new Intent(getApplicationContext(), ServicesActivity.class);
                    startActivity(intent);
                    print("Services");
                    finish();
                    break;
                case R.id.touchMeTextview:
                    print("head");
                    break;
                case R.id.imageViewExit:
                    finish();
                    System.exit(0);
                    print("exit");
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        textViewTouchMe = findViewById(R.id.touchMeTextview);
        Animation anim = new AlphaAnimation(0.0f, 1.5f);
        anim.setDuration(800); //You can manage the blinking time with this parameter
        anim.setStartOffset(0);
        anim.setRepeatMode(Animation.REVERSE);
        anim.setRepeatCount(Animation.INFINITE);
        textViewTouchMe.startAnimation(anim);

        cardViewConversation = findViewById(R.id.cardviewConversation);
        cardViewServices = findViewById(R.id.cardviewService);
        imageViewExit = findViewById(R.id.imageViewExit);

        cardViewConversation.setOnClickListener(onClickListener);
        cardViewServices.setOnClickListener(onClickListener);
        textViewTouchMe.setOnClickListener(onClickListener);
        imageViewExit.setOnClickListener(onClickListener);

    }


    private void print(String text){
        Log.d("Shish", text);
    }
}
