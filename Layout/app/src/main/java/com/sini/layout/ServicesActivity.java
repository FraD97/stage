package com.sini.layout;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.GridView;
import android.widget.ImageView;

import com.sini.layout.Model.CardviewAdapter;
import com.sini.layout.Model.Service;

import java.util.ArrayList;

public class ServicesActivity extends AppCompatActivity {

    private GridView listView;
    private ImageView backImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_services);

        listView = findViewById(R.id.listview_services);

        Service a = new Service(R.drawable.camera, "Camera");
        Service b = new Service(R.drawable.emotions, "Emozioni");
        Service c = new Service(R.drawable.dab, "Balliamo");
        Service d = new Service(R.drawable.boy, "Chi sei?");
        Service e = new Service(R.drawable.precision, "+ Riconoscimento");
        Service f = new Service(R.drawable.stats, "Statistiche");

        ArrayList<Service> list = new ArrayList<>();
        list.add(a);
        list.add(b);
        list.add(c);
        list.add(d);
        list.add(e);
        list.add(f);

        CardviewAdapter cardviewAdapter = new CardviewAdapter(this, list);
        listView.setAdapter(cardviewAdapter);
        backImage = findViewById(R.id.back_image);
        backImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(intent);
                ServicesActivity.this.finish();
            }
        });
    }
}
