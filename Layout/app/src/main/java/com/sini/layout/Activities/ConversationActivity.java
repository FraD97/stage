package com.sini.layout.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.sini.layout.MainActivity;
import com.sini.layout.R;

public class ConversationActivity extends AppCompatActivity {

    private ImageView backButton;
    private ImageView exitbutton;

    private View.OnClickListener onClickListener = new View.OnClickListener() {
        Intent intent = null;

        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.back_image:
                    intent = new Intent(getApplicationContext(), MainActivity.class);
                    startActivity(intent);
                    finish();
                    break;
                case R.id.imageViewExit:
                    finish();
                    System.exit(0);
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_conversation);

        exitbutton = findViewById(R.id.imageViewExit);
        backButton = findViewById(R.id.back_image);
        exitbutton.setOnClickListener(onClickListener);
        backButton.setOnClickListener(onClickListener);
    }
}
