-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Creato il: Set 02, 2019 alle 11:11
-- Versione del server: 5.7.26
-- Versione PHP: 7.2.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sanbot_db`
--
CREATE DATABASE IF NOT EXISTS `sanbot_db` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `sanbot_db`;

-- --------------------------------------------------------

--
-- Struttura della tabella `appartenenza_pa`
--

DROP TABLE IF EXISTS `appartenenza_pa`;
CREATE TABLE IF NOT EXISTS `appartenenza_pa` (
  `attributo` int(11) DEFAULT NULL,
  `persona` int(11) DEFAULT NULL,
  `valore` text,
  KEY `attributo` (`attributo`),
  KEY `persona` (`persona`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `appartenenza_pa`
--

INSERT INTO `appartenenza_pa` (`attributo`, `persona`, `valore`) VALUES
(1, 1, 'francesco'),
(2, 1, 'di sario'),
(3, 1, 'transessuale'),
(4, 1, 'diciotto'),
(1, 2, 'giuseppe'),
(2, 2, 'ignora ne'),
(3, 2, 'uomo'),
(4, 2, 'pen to know');

-- --------------------------------------------------------

--
-- Struttura della tabella `appartenenza_pe`
--

DROP TABLE IF EXISTS `appartenenza_pe`;
CREATE TABLE IF NOT EXISTS `appartenenza_pe` (
  `emozione` int(11) DEFAULT NULL,
  `persona` int(11) DEFAULT NULL,
  `data` date DEFAULT NULL,
  KEY `emozione` (`emozione`),
  KEY `persona` (`persona`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura della tabella `attributo`
--

DROP TABLE IF EXISTS `attributo`;
CREATE TABLE IF NOT EXISTS `attributo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) NOT NULL,
  `descrizione` text NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nome` (`nome`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `attributo`
--

INSERT INTO `attributo` (`id`, `nome`, `descrizione`) VALUES
(1, 'Nome', 'nome utente'),
(2, 'Cognome', 'cognome utente'),
(3, 'Età', 'età utente'),
(4, 'Compleanno', 'compleanno dell\'utente');

-- --------------------------------------------------------

--
-- Struttura della tabella `client`
--

DROP TABLE IF EXISTS `client`;
CREATE TABLE IF NOT EXISTS `client` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura della tabella `conversazione`
--

DROP TABLE IF EXISTS `conversazione`;
CREATE TABLE IF NOT EXISTS `conversazione` (
  `id` int(11) NOT NULL,
  `client` int(11) DEFAULT NULL,
  `persona` int(11) DEFAULT NULL,
  `data` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `client` (`client`),
  KEY `persona` (`persona`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura della tabella `domanda`
--

DROP TABLE IF EXISTS `domanda`;
CREATE TABLE IF NOT EXISTS `domanda` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `attributo` int(11) DEFAULT NULL,
  `testo` text,
  `confidenzialità` int(11) DEFAULT NULL,
  `tono` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `attributo` (`attributo`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `domanda`
--

INSERT INTO `domanda` (`id`, `attributo`, `testo`, `confidenzialità`, `tono`) VALUES
(1, 1, 'Come ti chiami?', 2, 2);

-- --------------------------------------------------------

--
-- Struttura della tabella `emozione`
--

DROP TABLE IF EXISTS `emozione`;
CREATE TABLE IF NOT EXISTS `emozione` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) NOT NULL,
  `livello` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nome` (`nome`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura della tabella `persona`
--

DROP TABLE IF EXISTS `persona`;
CREATE TABLE IF NOT EXISTS `persona` (
  `id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
