package com.sini.isanbot.Task;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;

import com.google.gson.JsonObject;
import com.sanbot.opensdk.function.unit.HDCameraManager;
import com.sini.isanbot.Helper.PhotoSender;
import com.sini.isanbot.Model.FrameData;
import com.sini.isanbot.Model.MessageAction;
import com.sini.isanbot.Model.ServerOption;
import com.sini.isanbot.Model.Service;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.Iterator;

import static com.sini.isanbot.Model.Utils.print;

public class EmotionTask implements Runnable {

    private Socket socket;
    private DataInputStream dataInputStream;
    private DataOutputStream dataOutputStream;
    private HDCameraManager hdCameraManager;
    private PhotoSender photoSender;
    private Handler handler;
    private boolean getFrame = false;
    private JSONObject response;

    public EmotionTask(HDCameraManager hdCameraManager) {
        this.hdCameraManager = hdCameraManager;
    }

    @Override
    public void run() {
        createSocket();
    }

    private void createSocket() {
        try {
            socket = new Socket(ServerOption.getInstance().getHost(), ServerOption.getInstance().getPort());
            photoSender = new PhotoSender(socket, hdCameraManager);
            photoSender.sendPhotoStream();
            while (true) {
                if (getJsonFromServer() && getFrame) {
                    break;
                }
            }
            sendDialogMessageToMainThread();
            socket.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void stopEmotion() {
        getFrame = true;
        photoSender.stopEmotion();
    }

    public void setHandler(Handler handler) {
        this.handler = handler;
    }

    private boolean getJsonFromServer() {
        try {
            print("OK");
            dataInputStream = new DataInputStream(socket.getInputStream());
            String json = dataInputStream.readUTF();
            JSONObject jsonObject = new JSONObject(json);
            response = jsonObject;
            if (jsonObject.getString("last").equals("false")) {
                sendMessageToMainThread(jsonObject);
                print("Messaggio inviato al main");
                return false;
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return true;
    }

    private void sendMessageToMainThread(JSONObject jsonObject) {
        FrameData frameData = new FrameData(jsonObject);
        Message message = new Message();
        if (frameData.isFaceDetected()) {
            message.what = MessageAction.UPDATE_USER_DATA;
            Bundle bundle = new Bundle();
            bundle.putString("eta", frameData.getAge());
            bundle.putString("etnia", frameData.getEthnicity());
            bundle.putString("sesso", frameData.getGender());
            bundle.putInt("gioia", frameData.getJoy());
            bundle.putInt("rabbia", frameData.getAnger());
            bundle.putInt("tristezza", frameData.getSadness());
            bundle.putInt("disgusto", frameData.getDisgust());
            bundle.putInt("disprezzo", frameData.getContempt());
            bundle.putInt("sorpresa", frameData.getSuprise());
            bundle.putInt("timore", frameData.getFear());
            bundle.putString("dominantEmotion", frameData.getDominantEmotion());
            message.setData(bundle);
        } else {
            message.what = MessageAction.FACE_NOT_DETECTED;
        }

        handler.sendMessage(message);
    }


    private void sendDialogMessageToMainThread() {
        Message message = new Message();
        message.what = MessageAction.CREATE_DIALOG_EMOTION;
        Bundle bundle = createBundle(response);
        message.setData(bundle);
        handler.sendMessage(message);
    }

    private Bundle createBundle(JSONObject jsonObject) {
        Iterator<String> iterator = jsonObject.keys();
        Bundle bundle = new Bundle();
        while (iterator.hasNext()) {
            String key = iterator.next();
            try {
                bundle.putString(key, jsonObject.get(key).toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return bundle;
    }
}
