package com.sini.isanbot.Helper;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;

import com.sini.isanbot.Model.MessageAction;

public class MessageSender {

    private static MessageSender messageSender;
    private Handler handler;

    private MessageSender(){}

    public static MessageSender getInstance(){
        if(messageSender == null){
            messageSender = new MessageSender();
        }
        return messageSender;
    }

    public void sendCustomMessage(int messageAction, String text){
        Message message = new Message();
        message.what = messageAction;
        Bundle bundle = new Bundle();
        bundle.putString("text", text);
        message.setData(bundle);
        handler.sendMessage(message);
    }

    public void sendMessage(int messageAction){
        Message message = new Message();
        message.what = messageAction;
        handler.sendMessage(message);
    }

    public void setHandler(Handler handler){
        this.handler = handler;
    }
}
