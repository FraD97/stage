package com.sini.isanbot;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.sanbot.opensdk.base.BindBaseActivity;
import com.sanbot.opensdk.beans.FuncConstant;
import com.sanbot.opensdk.function.beans.headmotion.LocateAbsoluteAngleHeadMotion;
import com.sanbot.opensdk.function.unit.HDCameraManager;
import com.sanbot.opensdk.function.unit.HardWareManager;
import com.sanbot.opensdk.function.unit.HeadMotionManager;
import com.sanbot.opensdk.function.unit.interfaces.hardware.TouchSensorListener;
import com.sini.isanbot.Helper.PhotoSender;
import com.sini.isanbot.Helper.VideoSender;
import com.sini.isanbot.Model.ServerOption;


import java.io.IOException;
import java.net.Socket;

import static com.sini.isanbot.Model.Utils.print;


public class CameraActivity extends BindBaseActivity {

    private HDCameraManager hdCameraManager;
    private Button buttonPhoto;
    private Button buttonVideo;
    private ImageView backImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        register(CameraActivity.class);
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON, WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setContentView(R.layout.activity_camera);

        hdCameraManager = (HDCameraManager) getUnitManager(FuncConstant.HDCAMERA_MANAGER);
        buttonPhoto = findViewById(R.id.buttonPhoto);
        buttonVideo = findViewById(R.id.buttonVideo);

        buttonPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Thread t = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        Socket socket = null;
                        try {
                            socket = new Socket(ServerOption.getInstance().getHost(), ServerOption.getInstance().getPort());
                            PhotoSender photoSender = new PhotoSender(socket, hdCameraManager);
                            photoSender.takeAndSendPhotoService();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                });
                t.start();
            }
        });

        buttonVideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Thread t = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        Socket socket = null;
                        try {
                            socket = new Socket(ServerOption.getInstance().getHost(), ServerOption.getInstance().getPort());
                            VideoSender videoSender = new VideoSender(socket, hdCameraManager);
                            videoSender.startVideo(10);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                });
                t.start();
            }
        });

        backImage = findViewById(R.id.back_image);
        backImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), ServicesActivity.class);
                startActivity(intent);
                CameraActivity.this.finish();
            }
        });
    }


    @Override
    protected void onMainServiceConnected() {

    }

    @Override
    protected void onPause() {
        finish();
        super.onPause();
    }

}
