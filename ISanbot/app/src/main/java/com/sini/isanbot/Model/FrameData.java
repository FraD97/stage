package com.sini.isanbot.Model;

import org.json.JSONException;
import org.json.JSONObject;

public class FrameData {

    private JSONObject jsonObject;
    private String gender;
    private String age;
    private boolean faceDetected;
    private String ethnicity;       //etnia
    private int joy;     //gioia
    private int anger;   //rabbia
    private int sadness;
    private int suprise;
    private int fear;    //paura
    private int contempt;       //disprezzo
    private int disgust;        //disgusto
    private int engagement;     //espressività
    private int valence;

    private String dominantEmotion;


    public FrameData(){

    }

    public FrameData(JSONObject jsonObject){
        try {
            this.jsonObject = jsonObject;
            this.gender = jsonObject.getString("gender");
            this.age = jsonObject.getString("age");
            this.ethnicity = jsonObject.getString("ethnicity");
            this.faceDetected = jsonObject.getBoolean("faceDetected");
            this.joy = jsonObject.getInt("joy");
            this.anger = jsonObject.getInt("anger");
            this.sadness = jsonObject.getInt("sadness");
            this.suprise = jsonObject.getInt("surprise");
            this.fear = jsonObject.getInt("fear");
            this.contempt = jsonObject.getInt("contempt");
            this.disgust = jsonObject.getInt("disgust");
            this.engagement = jsonObject.getInt("engagement");
            this.valence = jsonObject.getInt("valence");
            this.dominantEmotion = jsonObject.getString("dominantEmotion");

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public JSONObject getJsonObject() {
        return jsonObject;
    }

    @Override
    public String toString() {
        return "FrameData{" +
                "sex='" + gender + '\'' +
                "\n, age=" + age +
                "\n, faceDetected=" + faceDetected +
                "\n, ethnicity='" + ethnicity + '\'' +
                "\n, joy=" + joy +
                "\n, anger=" + anger +
                "\n, sadness=" + sadness +
                "\n, suprise=" + suprise +
                "\n, fear=" + fear +
                "\n, contempt=" + contempt +
                "\n, disgust=" + disgust +
                "\n, engagement=" + engagement +
                "\n, valence=" + valence +
                "}\n";
    }

    public String getDominantEmotion() {
        return dominantEmotion;
    }

    public void setJsonObject(JSONObject jsonObject) {
        this.jsonObject = jsonObject;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public boolean isFaceDetected() {
        return faceDetected;
    }

    public void setFaceDetected(boolean faceDetected) {
        this.faceDetected = faceDetected;
    }

    public String getEthnicity() {
        return ethnicity;
    }

    public void setEthnicity(String ethnicity) {
        this.ethnicity = ethnicity;
    }

    public int getJoy() {
        return joy;
    }

    public void setJoy(int joy) {
        this.joy = joy;
    }

    public int getAnger() {
        return anger;
    }

    public void setAnger(int anger) {
        this.anger = anger;
    }

    public int getSadness() {
        return sadness;
    }

    public void setSadness(int sadness) {
        this.sadness = sadness;
    }

    public int getSuprise() {
        return suprise;
    }

    public void setSuprise(int suprise) {
        this.suprise = suprise;
    }

    public int getFear() {
        return fear;
    }

    public void setFear(int fear) {
        this.fear = fear;
    }

    public int getContempt() {
        return contempt;
    }

    public void setContempt(int contempt) {
        this.contempt = contempt;
    }

    public int getDisgust() {
        return disgust;
    }

    public void setDisgust(int disgust) {
        this.disgust = disgust;
    }

    public int getEngagement() {
        return engagement;
    }

    public void setEngagement(int engagement) {
        this.engagement = engagement;
    }

    public int getValence() {
        return valence;
    }

    public void setValence(int valence) {
        this.valence = valence;
    }

}
