package com.sini.isanbot.Model;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;


import com.sini.isanbot.R;

import java.util.ArrayList;
import java.util.List;

public class CardviewAdapter extends ArrayAdapter<Service> {

    private Context mContext;
    private List<Service> serviceList = new ArrayList<>();


    public CardviewAdapter(@NonNull Context context, ArrayList<Service> list) {
        super(context, 0, list);
        mContext = context;
        serviceList = list;
    }

    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View listItem = convertView;
        if (listItem == null)
            listItem = LayoutInflater.from(mContext).inflate(R.layout.cardview_service, parent, false);

        Service currentService = serviceList.get(position);
        ImageView imageView = listItem.findViewById(R.id.image_service);
        TextView textView = listItem.findViewById(R.id.title_service);

        imageView.setImageResource(currentService.getImage_id());
        textView.setText(currentService.getTitle());

        return listItem;
    }

}
