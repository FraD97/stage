package com.sini.isanbot.Model;

public class MessageAction {

    public static final int  SHOOT_PHOTO = 0;
    public static final int  PHOTO_TAKEN = 1;
    public static final int  THINKING = 2;
    public static final int  CAN_LISTEN = 3;
    public static final int  LISTENING_STARTED = 4;
    public static final int  LISTENING_ENDED = 5;
    public static final int  UPDATE_SANBOT_TEXT = 6;
    public static final int  UPDATE_USER_TEXT = 7;
    public static final int  UPDATE_USERNAME = 8;
    public static final int  TALKING = 9;
    public static final int SIGNAL_PROBLEM = 10;
    public static final int NEW_PHOTO = 11;
    public static final int FACE_DETECTED = 12;
    public static final int FACE_NOT_DETECTED = 13;
    public static final int UPDATE_USER_DATA = 14;
    public static final int STOP_EMOTION_DETECTION = 15;
    public static final int CREATE_DIALOG_EMOTION = 16;
    public static final int UPDATE_SANBOT_EMOTION = 17;
}
