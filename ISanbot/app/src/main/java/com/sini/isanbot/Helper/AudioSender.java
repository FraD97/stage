package com.sini.isanbot.Helper;

import com.sanbot.opensdk.function.beans.StreamOption;
import com.sanbot.opensdk.function.unit.HDCameraManager;
import com.sanbot.opensdk.function.unit.interfaces.media.MediaStreamListener;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

import static com.sini.isanbot.Model.Utils.print;

public class AudioSender {

    private Socket socket;
    private HDCameraManager hdCameraManager;
    private boolean photoTaken = false;
    private boolean waitForPhoto = true;

    public AudioSender(Socket socket, HDCameraManager hdCameraManager){
        this.socket = socket;
        this.hdCameraManager = hdCameraManager;
    }


    public void recordAudioAndSend(){
        configureCamera();
        print("Camera configurata! Ora mando audio");
        while(waitForPhoto);
        waitForPhoto = true;
    }

    private void sendAudio(byte[] bytes){
        try {
            DataOutputStream dataOutputStream = new DataOutputStream(socket.getOutputStream());
            dataOutputStream.writeInt(bytes.length);
            dataOutputStream.write(bytes);
//            dataOutputStream.close();
        }
        catch (IOException e){
            print("Errore dataOutput stream: " + e.getMessage());
        }
    }


    private void configureCamera(){
        StreamOption streamOption = new StreamOption();
        streamOption.setDecodType(StreamOption.HARDWARE_DECODE);
        streamOption.setChannel(StreamOption.MAIN_STREAM);
        hdCameraManager.setMediaListener(new MediaStreamListener() {
            @Override
            public void getVideoStream(int i, byte[] bytes, int i1, int i2) {

            }

            @Override
            public void getAudioStream(int i, byte[] bytes) {
               print("listener: " + bytes.length);
               sendAudio(bytes);
            }
        });
        hdCameraManager.openStream(streamOption);
    }

}
