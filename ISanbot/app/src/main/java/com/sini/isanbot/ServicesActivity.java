package com.sini.isanbot;

import android.content.Intent;
import android.graphics.Camera;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;


import com.sanbot.opensdk.base.BindBaseActivity;
import com.sini.isanbot.Model.CardviewAdapter;
import com.sini.isanbot.Model.Service;

import java.util.ArrayList;

import static com.sini.isanbot.Model.Utils.print;

public class ServicesActivity extends BindBaseActivity {

    private GridView listView;
    private ImageView backImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        register(ServicesActivity.class);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_services);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON, WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        listView = findViewById(R.id.listview_services);
        Service a = new Service(R.drawable.camera, "Camera");
        Service b = new Service(R.drawable.emotions, "Emozioni");
        Service c = new Service(R.drawable.dab, "Balliamo");
        Service d = new Service(R.drawable.boy, "Chi sei?");
        Service e = new Service(R.drawable.precision, "+ Riconoscimento");
        Service f = new Service(R.drawable.stats, "Statistiche");

        ArrayList<Service> list = new ArrayList<>();
        list.add(a);
        list.add(b);
        list.add(c);
        list.add(d);
        list.add(e);
        list.add(f);

        CardviewAdapter cardviewAdapter = new CardviewAdapter(this, list);
        listView.setAdapter(cardviewAdapter);
        backImage = findViewById(R.id.back_image);
        backImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(intent);
                ServicesActivity.this.finish();
            }
        });
        listView.setClickable(true);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position,
                                    long id) {
                Service service = (Service) listView.getItemAtPosition(position);
                Intent intent = null;
                switch (service.getTitle()) {
                    case "Camera":
                        intent = new Intent(getApplicationContext(), CameraActivity.class);
                        startActivity(intent);
                        finish();
                        break;
                    case "Emozioni":
                        intent = new Intent(getApplicationContext(), EmotionActivity.class);
                        startActivity(intent);
                        finish();
                        break;
                }
            }
        });
    }

    @Override
    protected void onMainServiceConnected() {

    }
}
