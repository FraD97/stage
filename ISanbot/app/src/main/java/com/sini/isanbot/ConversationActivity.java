package com.sini.isanbot;


import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.sanbot.opensdk.base.BindBaseActivity;
import com.sanbot.opensdk.beans.FuncConstant;
import com.sanbot.opensdk.function.unit.HDCameraManager;
import com.sanbot.opensdk.function.unit.HandMotionManager;
import com.sanbot.opensdk.function.unit.HardWareManager;
import com.sanbot.opensdk.function.unit.HeadMotionManager;
import com.sanbot.opensdk.function.unit.SpeechManager;
import com.sanbot.opensdk.function.unit.SystemManager;
import com.sini.isanbot.Manager.InteractionManager;
import com.sini.isanbot.Manager.SanbotManager;
import com.sini.isanbot.Task.ConversationTask;

import static com.sini.isanbot.Model.Utils.print;
import static com.sini.isanbot.Model.MessageAction.*;


public class ConversationActivity extends BindBaseActivity {
    private HeadMotionManager headMotionManager;
    private SpeechManager speechManager;
    private HDCameraManager hdCameraManager;
    private HardWareManager hardWareManager;
    private SystemManager systemManager;
    private HandMotionManager handMotionManager;
    private InteractionManager interactionManager;

    SanbotManager sanbotManager = new SanbotManager();
    private ImageView backButton;
    private ImageView exitbutton;
    private ConversationTask conversationTask;

    private ImageView imageFeedback;
    private TextView textViewFeedback;
    private TextView textViewProblem;
    private TextView textViewSanbot;
    private TextView textViewUser;
    private ProgressBar progressBar;


    private Handler handler = null;
    private Handler taskHandler = null;

    private View.OnClickListener onClickListener = new View.OnClickListener() {
        Intent intent = null;

        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.back_image:
                    intent = new Intent(getApplicationContext(), MainActivity.class);
                    startActivity(intent);
                    conversationTask.setRunning(false);
                    finish();
                    break;
                case R.id.imageViewExit:
                    finish();
                    System.exit(0);
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        register(ConversationActivity.class);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_conversation);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON, WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        exitbutton = findViewById(R.id.imageViewExit);
        backButton = findViewById(R.id.back_image);
        exitbutton.setOnClickListener(onClickListener);
        backButton.setOnClickListener(onClickListener);
        imageFeedback = findViewById(R.id.image_feedback);
        textViewFeedback = findViewById(R.id.textview_feedback);
        textViewProblem = findViewById(R.id.textview_problem);
        textViewSanbot = findViewById(R.id.textview_sanbot);
        textViewUser = findViewById(R.id.tetview_user);
        progressBar = findViewById(R.id.progressBar);

        createHandler();


        textViewProblem.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                imageFeedback.setImageResource(R.drawable.camera);
                textViewFeedback.setText("Preparazione fotocamera");
                Message message = new Message();
                message.what = NEW_PHOTO;
                taskHandler.sendMessage(message);
                textViewProblem.setVisibility(View.GONE);
                textViewFeedback.setVisibility(View.VISIBLE);
            }
        });
    }

    @Override
    protected void onPause() {
        finish();
        super.onPause();
    }

    @Override
    protected void onMainServiceConnected() {
        initManager();
        startConversation();
    }

    private void startConversation() {
        conversationTask = new ConversationTask(sanbotManager, handler, this);
        SharedPreferences sharedPreferences = this.getSharedPreferences("com.sini.isanbot", Context.MODE_PRIVATE);

        conversationTask.setClientID(sharedPreferences.getInt("clientID", 0));
        conversationTask.setUsername(sharedPreferences.getString("clientName", ""));

        conversationTask.setContext(this);
        conversationTask.setInteractionManager(interactionManager);
        Thread t = new Thread(conversationTask);
        t.start();
    }

    private void initManager() {
        hardWareManager = (HardWareManager) getUnitManager(FuncConstant.HARDWARE_MANAGER);
        headMotionManager = (HeadMotionManager) getUnitManager(FuncConstant.HEADMOTION_MANAGER);
        speechManager = (SpeechManager) getUnitManager(FuncConstant.SPEECH_MANAGER);
        systemManager = (SystemManager) getUnitManager(FuncConstant.SYSTEM_MANAGER);
        handMotionManager = (HandMotionManager)getUnitManager(FuncConstant.HANDMOTION_MANAGER);
        hdCameraManager = (HDCameraManager) getUnitManager(FuncConstant.HDCAMERA_MANAGER);
        sanbotManager.setHardWareManager(hardWareManager);
        sanbotManager.setHeadMotionManager(headMotionManager);
        sanbotManager.setSpeechManager(speechManager);
        sanbotManager.setHdCameraManager(hdCameraManager);
        interactionManager = new InteractionManager(systemManager, hardWareManager, handMotionManager);

    }


    private void createHandler() {
        handler = new Handler(Looper.getMainLooper()) {
            @Override
            public void handleMessage(Message msg) {
                print("" + msg.what);
                switch (msg.what) {
                    case SHOOT_PHOTO:
                        giveUserFeedback(R.drawable.camera, "Preparazione fotocamera");
                        break;
                    case PHOTO_TAKEN:
                        giveUserFeedback(R.drawable.camera, "Foto scattata!");
                        break;
                    case THINKING:
                        giveUserFeedback();
                        break;
                    case CAN_LISTEN:
                        giveUserFeedback(R.drawable.microphone, "Dimmi qualcosa");
                        break;
                    case LISTENING_STARTED:
                        giveUserFeedback(R.drawable.microphone_blue, "Voce rilevata, sto ascoltando");
                        break;
                    case LISTENING_ENDED:
                        giveUserFeedback(R.drawable.microphone_blue, "Fine dialogo");
                        break;
                    case TALKING:
                        giveUserFeedback(R.drawable.speak, "Sto parlando");
                        break;
                    case UPDATE_SANBOT_TEXT:
                        setChatText(msg, false);
                        break;
                    case UPDATE_USER_TEXT:
                        setChatText(msg, true);
                        break;
                    case SIGNAL_PROBLEM:
                        handleProblem();
                }
            }

            public void giveUserFeedback(int imgRes, String text) {
                if (progressBar.getVisibility() == View.VISIBLE) {
                    progressBar.setVisibility(View.GONE);
                    imageFeedback.setVisibility(View.VISIBLE);
                }
                imageFeedback.setImageResource(imgRes);
                textViewFeedback.setText(text);
            }

            //mostra una progress bar
            public void giveUserFeedback() {
                imageFeedback.setVisibility(View.GONE);
                progressBar.setVisibility(View.VISIBLE);
                textViewFeedback.setText("Sto pensando...");

            }

            public void setChatText(Message message, boolean flag) {
                Bundle bundle = message.getData();
                String text = bundle.getString("text");
                if (flag) {
                    textViewUser.setText(text);
                } else {
                    textViewSanbot.setText(text);
                }
            }

            public void handleProblem(){
                textViewFeedback.setVisibility(View.GONE);
                textViewProblem.setVisibility(View.VISIBLE);
                imageFeedback.setImageResource(R.drawable.confused);
                imageFeedback.setVisibility(View.VISIBLE);
                progressBar.setVisibility(View.GONE);
            }
        };
    }

    public void setTaskHandler(Handler handler){
        this.taskHandler = handler;
    }
}
