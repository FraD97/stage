package com.sini.isanbot.Helper;


import com.sini.isanbot.Helper.MessageSender;
import com.sini.isanbot.Manager.AudioManager;
import com.sini.isanbot.Model.MessageAction;

import static com.sini.isanbot.Model.Utils.print;

public class VoiceDetector implements Runnable {

    private AudioManager audioManager;
    private MessageSender messageSender = MessageSender.getInstance();

    private double micValue = 0;
    private int silenceCounter = 0;
    private int voiceCounter = 0;

    private long audioStartedAt = 0;
    private long voiceDetectedAt = 0;
    private int cut = 0;
    boolean alreadyDone = false;

    public VoiceDetector(AudioManager audioManager) {
        this.audioManager = audioManager;
    }

    @Override
    public void run() {
        audioManager.startRegister();
        audioStartedAt = System.currentTimeMillis();
        detectNoise();
        detectSilence();
    }

    private void detectNoise(){
        while (true){
            if(!alreadyDone) {
                messageSender.sendMessage(MessageAction.CAN_LISTEN);
                alreadyDone = true;
            }
            micValue = audioManager.getMicrophoneValues();
//            print("valore: " + micValue);
            if(micValue > 7000){
                voiceDetectedAt = System.currentTimeMillis();
                print("Rumore rilevato");
                if(voiceDetection()){
                    messageSender.sendMessage(MessageAction.LISTENING_STARTED);
                    print("è una voce");
                    voiceCounter = 0;
                    break;
                }
                else{
                    print("non è una voce, era un rumore");
                    voiceCounter = 0;
                    alreadyDone = false;
                }
            }
            try {
                Thread.sleep(200);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private boolean voiceDetection(){
        int shots = 10;
        while(shots >= 0){
            micValue = audioManager.getMicrophoneValues();
            print("Valore del micValue: " + micValue);
            if (micValue > 5000) {
                voiceCounter++;
            }
            shots--;
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        return voiceCounter >= 3;
    }



    private void detectSilence() {
        print("Sto analizzando il silenzion");
        while (true) {
            micValue = audioManager.getMicrophoneValues();
//            print("valore: " + micValue);
            if (micValue < 5000) {
                silenceCounter++;
            } else {
                silenceCounter = 0;
            }
            if (silenceCounter > 8) {
                silenceCounter = 0;
                print("Hai smesso di parlare.. FINE RECORDING");
                messageSender.sendMessage(MessageAction.LISTENING_ENDED);
                audioManager.stopRegister();
                cut = (int) ((voiceDetectedAt - audioStartedAt) / 1000) - 1;
                if (cut < 0) {
                    cut = 0;
                }
                break;
            }
            try {
                Thread.sleep(200);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public int getCut() {
        return cut;
    }
}
