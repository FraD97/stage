package com.sini.isanbot.Model;

public class ServiceID {

    public static final int login = 10;
    public static final int conversation = 20;
    public static final int photo = 30;
    public static final int video = 40;
    public static final int improveFaceRecognition = 50;
    public static final int failure = -1;
    public static final int success = 200;
    public static final int emotionDetection = 60;
}
