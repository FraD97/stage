package com.sini.isanbot.Task;

import com.sanbot.opensdk.function.beans.speech.SpeakStatus;
import com.sanbot.opensdk.function.unit.SpeechManager;
import com.sanbot.opensdk.function.unit.interfaces.speech.SpeakListener;

import static com.sini.isanbot.Model.Utils.print;

public class Speaker implements SpeakListener {

    private SpeechManager speechManager;
    private boolean isSpeaking = true;
    private int count = 0;

    public Speaker(SpeechManager speechManager) {
        this.speechManager = speechManager;
        speechManager.setOnSpeechListener(this);
    }


    @Override
    public void onSpeakStatus(SpeakStatus speakStatus) {
        count++;
        if (count > 1) {
            isSpeaking = false;
            count = 0;
        }
    }

    public void speak(String text) {
        speechManager.startSpeak(text);
        while (isSpeaking) {
        } //do noting
        isSpeaking = true;
    }
}
