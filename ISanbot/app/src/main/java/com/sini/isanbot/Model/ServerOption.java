package com.sini.isanbot.Model;


public class ServerOption {

    private static ServerOption serveroption;
    private String host;
    private int port;

    private ServerOption() {}

    public static synchronized ServerOption getInstance() {
        if(serveroption == null){
            serveroption = new ServerOption();
        }
        return serveroption;
    }

    public void setHost(String host) {
        serveroption.host = host;
    }

    public void setPort(int port) {
        serveroption.port = port;
    }

    public String getHost() {
        return serveroption.host;
    }

    public int getPort() {
        return serveroption.port;
    }


}
