package com.sini.isanbot;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;
import androidx.appcompat.app.AppCompatActivity;

import com.sanbot.opensdk.base.BindBaseActivity;
import com.sini.isanbot.Model.ServerOption;
import com.sini.isanbot.Model.ServiceID;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Inet4Address;
import java.net.Socket;
import java.net.UnknownHostException;

import static com.sini.isanbot.Model.Utils.print;


public class LoginActivity extends BindBaseActivity {

    private TextView confirmTextView;
    private TextView userTextView;
    private TextView passwordTextView;
    private int result = 0;
    private String username;
    private int clientID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        register(LoginActivity.class);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON, WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        userTextView = findViewById(R.id.textview_user);
        confirmTextView = findViewById(R.id.confirmTextview);
        passwordTextView = findViewById(R.id.passwordTextview);
        setServerParameters("172.16.48.187", 8189);
        confirmTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (userTextView.getText().length() > 2) {
                    requestLogin();
                    if (result == ServiceID.success) {
                        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                        startActivity(intent);
                        finish();
                    }
                }
            }
        });
        checkSession();
    }

    // crea un oggetto json e lo manda al server
    private void requestLogin() {
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("service", ServiceID.login);
                    jsonObject.put("client", userTextView.getText());

                    Socket socket = new Socket(ServerOption.getInstance().getHost(), ServerOption.getInstance().getPort());
                    DataOutputStream dataOutputStream = new DataOutputStream(socket.getOutputStream());
                    DataInputStream dataInputStream = new DataInputStream(socket.getInputStream());
                    dataOutputStream.writeUTF(jsonObject.toString());

                    print("In attesa di risposta dal server...");

                    String response = dataInputStream.readUTF();

                    print("Risposta dal server: " + response);
                    jsonObject = new JSONObject(response);

                    result = jsonObject.getInt("responseCode");
                    username = jsonObject.getString("client");
                    clientID = jsonObject.getInt("userID");

                    dataInputStream.close();
                    dataOutputStream.close();
                    socket.close();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
        t.start();
        try {
            t.join();
            createSession();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void setServerParameters(String host, int port) {
        ServerOption serverOption = ServerOption.getInstance();
        serverOption.setHost(host);
        serverOption.setPort(port);
    }

    private void createSession() {
        SharedPreferences sharedPref = this.getSharedPreferences("com.sini.isanbot", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString("clientName", username);
        editor.putInt("clientID", clientID);
        editor.apply();
    }

    private void checkSession(){
        SharedPreferences sharedPref = this.getSharedPreferences("com.sini.isanbot", Context.MODE_PRIVATE);
        clientID = sharedPref.getInt("clientID", 0);
        username = sharedPref.getString("clientName", "");
        if(clientID != 0){
            userTextView.setText(username);
            passwordTextView.setText("******");
        }
    }

    @Override
    protected void onMainServiceConnected() {

    }
}
