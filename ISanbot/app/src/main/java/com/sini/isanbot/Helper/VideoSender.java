package com.sini.isanbot.Helper;

import com.sanbot.opensdk.function.beans.StreamOption;
import com.sanbot.opensdk.function.unit.HDCameraManager;
import com.sanbot.opensdk.function.unit.interfaces.media.MediaStreamListener;
import com.sini.isanbot.Model.ServiceID;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

import static com.sini.isanbot.Model.Utils.print;

public class VideoSender {

    private Socket socket;
    private HDCameraManager hdCameraManager;
    int numberOfFrames = 0;
    int count = 0;

    public VideoSender(Socket socket, HDCameraManager hdCameraManager){
        this.socket = socket;
        this.hdCameraManager = hdCameraManager;
    }


    public void startVideo(int seconds){
        numberOfFrames = seconds * 5;
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("service", ServiceID.video);
            jsonObject.put("user", "");
            DataOutputStream dataOutputStream = null;
            dataOutputStream = new DataOutputStream(socket.getOutputStream());
            dataOutputStream.writeUTF(jsonObject.toString());
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        configureCamera();
    }

    private void sendFrame(byte[] bytes){
        try {
            DataOutputStream dataOutputStream = new DataOutputStream(socket.getOutputStream());
            dataOutputStream.writeInt(bytes.length);
            dataOutputStream.write(bytes);
        }
        catch (IOException e){
            print("Errore dataOutput stream: " + e.getMessage());
        }
    }


    private void configureCamera(){
        StreamOption streamOption = new StreamOption();
        streamOption.setDecodType(StreamOption.HARDWARE_DECODE);
        streamOption.setChannel(StreamOption.MAIN_STREAM);
        streamOption.setJustIframe(true);
        hdCameraManager.setMediaListener(new MediaStreamListener() {
            @Override
            public void getVideoStream(int i, byte[] bytes, int i1, int i2) {
                if(count < numberOfFrames){
                    print("True");
                    sendFrame(bytes);
                    count++;
                }
                else{
                    print("Finish");
                    hdCameraManager.closeStream(i);
                    try {
                        socket.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void getAudioStream(int i, byte[] bytes) {

            }
        });
        hdCameraManager.openStream(streamOption);
    }

}
