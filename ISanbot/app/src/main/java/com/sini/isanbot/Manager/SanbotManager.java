package com.sini.isanbot.Manager;

import com.sanbot.opensdk.function.unit.HDCameraManager;
import com.sanbot.opensdk.function.unit.HardWareManager;
import com.sanbot.opensdk.function.unit.HeadMotionManager;
import com.sanbot.opensdk.function.unit.SpeechManager;

public class SanbotManager {
    private HDCameraManager hdCameraManager = null;
    private HeadMotionManager headMotionManager = null;
    private HardWareManager hardWareManager = null;
    private SpeechManager speechManager = null;

    public void setHdCameraManager(HDCameraManager hdCameraManager) {
        this.hdCameraManager = hdCameraManager;
    }

    public void setHeadMotionManager(HeadMotionManager headMotionManager) {
        this.headMotionManager = headMotionManager;
    }

    public void setHardWareManager(HardWareManager hardWareManager) {
        this.hardWareManager = hardWareManager;
    }

    public void setSpeechManager(SpeechManager speechManager) {
        this.speechManager = speechManager;
    }


    public HDCameraManager getHdCameraManager() {
        return hdCameraManager;
    }

    public HeadMotionManager getHeadMotionManager() {
        return headMotionManager;
    }

    public HardWareManager getHardWareManager() {
        return hardWareManager;
    }

    public SpeechManager getSpeechManager() {
        return speechManager;
    }
}
