package com.sini.isanbot;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.sanbot.opensdk.base.BindBaseActivity;
import com.sanbot.opensdk.beans.FuncConstant;
import com.sanbot.opensdk.function.beans.EmotionsType;
import com.sanbot.opensdk.function.beans.LED;
import com.sanbot.opensdk.function.beans.handmotion.RelativeAngleHandMotion;
import com.sanbot.opensdk.function.beans.headmotion.LocateAbsoluteAngleHeadMotion;
import com.sanbot.opensdk.function.unit.HDCameraManager;
import com.sanbot.opensdk.function.unit.HandMotionManager;
import com.sanbot.opensdk.function.unit.HardWareManager;
import com.sanbot.opensdk.function.unit.HeadMotionManager;
import com.sanbot.opensdk.function.unit.SpeechManager;
import com.sanbot.opensdk.function.unit.SystemManager;
import com.sanbot.opensdk.function.unit.interfaces.hardware.TouchSensorListener;
import com.sini.isanbot.Helper.PhotoSender;
import com.sini.isanbot.Helper.VideoSender;
import com.sini.isanbot.Manager.HeadManager;
import com.sini.isanbot.Manager.InteractionManager;
import com.sini.isanbot.Model.ServerOption;
import com.sini.isanbot.Task.EmotionTask;


import java.io.IOException;
import java.net.Socket;
import java.util.Timer;
import java.util.TimerTask;

import static com.sini.isanbot.Model.MessageAction.CAN_LISTEN;
import static com.sini.isanbot.Model.MessageAction.CREATE_DIALOG_EMOTION;
import static com.sini.isanbot.Model.MessageAction.FACE_DETECTED;
import static com.sini.isanbot.Model.MessageAction.FACE_NOT_DETECTED;
import static com.sini.isanbot.Model.MessageAction.LISTENING_ENDED;
import static com.sini.isanbot.Model.MessageAction.LISTENING_STARTED;
import static com.sini.isanbot.Model.MessageAction.PHOTO_TAKEN;
import static com.sini.isanbot.Model.MessageAction.SHOOT_PHOTO;
import static com.sini.isanbot.Model.MessageAction.SIGNAL_PROBLEM;
import static com.sini.isanbot.Model.MessageAction.STOP_EMOTION_DETECTION;
import static com.sini.isanbot.Model.MessageAction.TALKING;
import static com.sini.isanbot.Model.MessageAction.THINKING;
import static com.sini.isanbot.Model.MessageAction.UPDATE_SANBOT_TEXT;
import static com.sini.isanbot.Model.MessageAction.UPDATE_USER_DATA;
import static com.sini.isanbot.Model.MessageAction.UPDATE_USER_TEXT;
import static com.sini.isanbot.Model.Utils.print;


public class EmotionActivity extends BindBaseActivity {

    private HDCameraManager hdCameraManager;
    private HardWareManager hardWareManager;
    private HeadManager headManager;
    private SystemManager systemManager;
    private HandMotionManager handMotionManager;
    private InteractionManager interactionManager;
    private SpeechManager speechManager;
    private ImageView backImage;
    private EmotionTask emotionTask;
    private Handler handler;


    private TextView textViewFace;
    private TextView textViewEta;
    private TextView textViewGender;
    private TextView textViewEthnicity;
    private TextView textViewJoy;
    private TextView textViewAnger;
    private TextView textViewSadness;
    private TextView textViewDisgust;
    private TextView textViewContempt;
    private TextView textViewSurprise;
    private TextView textViewFear;

    private TextView titleEmotion;
    private ImageView imageEmotion;

    private TextView startButton;
    private boolean startEmotionDetection = true;
    private TextView sanbotTextView;
    private ViewDialog viewDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        register(EmotionActivity.class);
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON, WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setContentView(R.layout.activity_emotion);

        createHandler();

        hdCameraManager = (HDCameraManager) getUnitManager(FuncConstant.HDCAMERA_MANAGER);
        backImage = findViewById(R.id.back_image);
        backImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), ServicesActivity.class);
                startActivity(intent);
                EmotionActivity.this.finish();
            }
        });

        startButton = findViewById(R.id.startButton);
        startButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (startEmotionDetection) {
                    headManager.moveHeadUp();
                    speechManager.startSpeak("Rilevamento emozioni in corso!");
                    sanbotTextView.setText("Rilevamento emozioni in corso");
                    emotionTask = new EmotionTask(hdCameraManager);
                    emotionTask.setHandler(handler);
                    Thread thread = new Thread(emotionTask);
                    thread.start();
                    print("Thread partito");
                    startButton.setText("STOP");
                    startEmotionDetection = false;
                } else {
                    headManager.moveHeadDown();
                    startButton.setEnabled(false);
                    emotionTask.stopEmotion();
                    startEmotionDetection = true;
                    startButton.setText("Start");
                    resetStatus(false);
                }
            }
        });

        textViewFace = findViewById(R.id.textViewFace);
        textViewEta = findViewById(R.id.textViewEta);
        textViewEthnicity = findViewById(R.id.textViewEtnia);
        textViewGender = findViewById(R.id.textViewSesso);
        textViewJoy = findViewById(R.id.textViewJoy);
        textViewAnger = findViewById(R.id.textViewAnger);
        textViewSadness = findViewById(R.id.textViewSadness);
        textViewDisgust = findViewById(R.id.textViewDisgust);
        textViewContempt = findViewById(R.id.textViewContempt);
        textViewSurprise = findViewById(R.id.textViewSurprise);
        textViewFear = findViewById(R.id.textViewFear);
        titleEmotion = findViewById(R.id.title_emotion);
        imageEmotion = findViewById(R.id.image_emotion);
        sanbotTextView = findViewById(R.id.textview_sanbot);

    }


    private void resetStatus(boolean flag) {
        RelativeAngleHandMotion relativeAngleHandMotion = new RelativeAngleHandMotion(RelativeAngleHandMotion.PART_LEFT, 5,
                RelativeAngleHandMotion.ACTION_UP,
                90
        );
        interactionManager.moveArm(relativeAngleHandMotion);
        if (flag) {
            speechManager.startSpeak("Ciao, Ora rilevero le tue emozioni. Premi Start Quando sei pronto.");
        } else {
            speechManager.startSpeak("Rilevamento terminato. Purtroppo a volte mi sbaglio, se hai voglia lascia un voto");
            sanbotTextView.setText("Rilevamento finito. Premi start per rieseguire.");
        }

        interactionManager.setEmotion(EmotionsType.SMILE);
    }

    @Override
    protected void onMainServiceConnected() {
        HeadMotionManager headMotionManager = (HeadMotionManager) getUnitManager(FuncConstant.HEADMOTION_MANAGER);
        headManager = new HeadManager(headMotionManager);
        speechManager = (SpeechManager) getUnitManager(FuncConstant.SPEECH_MANAGER);
        handMotionManager = (HandMotionManager) getUnitManager(FuncConstant.HANDMOTION_MANAGER);
        hardWareManager = (HardWareManager) getUnitManager(FuncConstant.HARDWARE_MANAGER);
        systemManager = (SystemManager) getUnitManager(FuncConstant.SYSTEM_MANAGER);

        interactionManager = new InteractionManager(systemManager, hardWareManager, handMotionManager);
        interactionManager.setLedColor(new LED(LED.PART_ALL, LED.MODE_WHITE));
        interactionManager.setEmotion(EmotionsType.SMILE);
        resetStatus(true);
    }

    @Override
    protected void onPause() {
        finish();
        super.onPause();
    }


    private void createHandler() {
        handler = new Handler(Looper.getMainLooper()) {
            @Override
            public void handleMessage(Message msg) {
                print("Messaggio ricevuto");
                switch (msg.what) {
                    case UPDATE_USER_DATA:
                        updateUserData(msg.getData());
                        break;
                    case FACE_NOT_DETECTED:
                        updateView("VOLTO NON RILEVATO", 0);
                        break;
                    case STOP_EMOTION_DETECTION:
                        updateView("Volto non rilevato", 1);
                        break;
                    case CREATE_DIALOG_EMOTION:
                        startDialog(msg.getData());
                        break;
                }
            }

            private void updateView(String text, int action) {
                textViewFace.setText(text);
                imageEmotion.setImageResource(R.drawable.warning);
                titleEmotion.setText("Volto non rilevato");
                interactionManager.setLedColor(new LED(LED.PART_ALL, LED.MODE_RED));
                if (action == 0) {
                    textViewFace.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.red));
                } else {
                    textViewFace.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.green));
                }
            }

            private void updateUserData(Bundle data) {
                String gender, ethnicity, age, dominantEmotion;
                int joy, anger, sadness, disgust, contempt, surprise, fear;

                gender = data.getString("sesso");
                print("Sesso: " + gender);
                ethnicity = data.getString("etnia");
                age = data.getString("eta");
                dominantEmotion = data.getString("dominantEmotion");

                joy = data.getInt("gioia");
                anger = data.getInt("rabbia");
                sadness = data.getInt("tristezza");
                disgust = data.getInt("disgusto");
                contempt = data.getInt("disprezzo");
                surprise = data.getInt("sorpresa");
                fear = data.getInt("timore");

                textViewFace.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.green));
                interactionManager.setLedColor(new LED(LED.PART_ALL, LED.MODE_GREEN));
                textViewFace.setText("Volto Rilevato");
                textViewEta.setText(age);
                textViewGender.setText(gender);
                textViewEthnicity.setText(ethnicity);
                textViewJoy.setText("" + joy);
                textViewAnger.setText("" + anger);
                textViewSadness.setText("" + sadness);
                textViewDisgust.setText("" + disgust);
                textViewContempt.setText("" + contempt);
                textViewSurprise.setText("" + surprise);
                textViewFear.setText("" + fear);

                if (dominantEmotion != null) {
                    getEmotionFeedback(dominantEmotion);
                } else {
                    interactionManager.setEmotion(EmotionsType.NORMAL);
                    imageEmotion.setImageResource(R.drawable.neutral);
                    titleEmotion.setText("Neutrale");
                }
            }

            private void getEmotionFeedback(String dominantEmotion) {
                switch (dominantEmotion) {
                    case "joy":
                        imageEmotion.setImageResource(R.drawable.joy);
                        titleEmotion.setText("Gioia");
                        interactionManager.setEmotion(EmotionsType.SMILE);
                        break;
                    case "anger":
                        imageEmotion.setImageResource(R.drawable.anger);
                        titleEmotion.setText("Rabbia");
                        interactionManager.setEmotion(EmotionsType.ANGRY);
                        break;
                    case "sadness":
                        imageEmotion.setImageResource(R.drawable.sad);
                        titleEmotion.setText("Tristezza");
                        interactionManager.setEmotion(EmotionsType.CRY);
                        break;
                    case "disgust":
                        imageEmotion.setImageResource(R.drawable.disgust);
                        titleEmotion.setText("Disgusto");
                        interactionManager.setEmotion(EmotionsType.ABUSE);
                        break;
                    case "contempt":
                        imageEmotion.setImageResource(R.drawable.contempt);
                        titleEmotion.setText("Disprezzo");
                        interactionManager.setEmotion(EmotionsType.QUESTION);
                        break;
                    case "surprise":
                        imageEmotion.setImageResource(R.drawable.surprise);
                        titleEmotion.setText("Sorpresa");
                        interactionManager.setEmotion(EmotionsType.SURPRISE);
                        break;
                    case "fear":
                        imageEmotion.setImageResource(R.drawable.scared);
                        titleEmotion.setText("Paura");
                        interactionManager.setEmotion(EmotionsType.FAINT);
                        break;
                }
            }

            private void startDialog(Bundle data) {
                viewDialog = new ViewDialog(EmotionActivity.this);
                viewDialog.setAge(data.getString("age"));
                viewDialog.setPropabilityAge(data.getString("probabilityAge"));
                viewDialog.setGender(data.getString("gender"));
                viewDialog.setEthnicity(data.getString("ethnicity"));
                viewDialog.setProbabilityEthnicity(data.getString("probabilityEthnicity"));
                String[] emotions = new String[3];
                emotions[0] = data.getString("firstEmotion");
                emotions[1] = data.getString("secondEmotion");
                emotions[2] = data.getString("thirdEmotion");
                viewDialog.setEmotions(emotions);
                viewDialog.show();
                startButton.setEnabled(true);
            }
        };

    }
}
