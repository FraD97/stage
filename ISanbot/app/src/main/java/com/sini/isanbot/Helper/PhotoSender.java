package com.sini.isanbot.Helper;

import com.google.gson.JsonIOException;
import com.sanbot.opensdk.function.beans.StreamOption;
import com.sanbot.opensdk.function.unit.HDCameraManager;
import com.sanbot.opensdk.function.unit.interfaces.media.MediaStreamListener;
import com.sini.isanbot.Model.ServiceID;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

import static com.sini.isanbot.Model.Utils.print;

public class PhotoSender {

    private Socket socket;
    private HDCameraManager hdCameraManager;
    private boolean photoTaken = false;
    private boolean waitForPhoto = true;
    private DataOutputStream dataOutputStream;
    private DataInputStream dataInputStream;

    boolean stopEmotion = false;

    public PhotoSender(Socket socket, HDCameraManager hdCameraManager) {
        this.socket = socket;
        this.hdCameraManager = hdCameraManager;
    }


    public void takeAndSendPhotoService() {
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("service", ServiceID.photo);
            jsonObject.put("client", "");
            DataOutputStream dataOutputStream = null;
            dataOutputStream = new DataOutputStream(socket.getOutputStream());
            dataOutputStream.writeUTF(jsonObject.toString());
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        configureCamera();
        while (waitForPhoto) ;
        waitForPhoto = true;
    }

    public void takeAndSendPhoto() {
        configureCamera();
        while (waitForPhoto);
        waitForPhoto = true;
    }

    private void sendPhoto(byte[] bytes) {
        try {
            DataOutputStream dataOutputStream = new DataOutputStream(socket.getOutputStream());
            dataOutputStream.writeInt(bytes.length);
            dataOutputStream.write(bytes);
//            dataOutputStream.close();
        } catch (IOException e) {
            print("Errore dataOutput stream: " + e.getMessage());
        }
    }


    private void configureCamera() {
        StreamOption streamOption = new StreamOption();
        streamOption.setDecodType(StreamOption.HARDWARE_DECODE);
        streamOption.setChannel(StreamOption.MAIN_STREAM);
        hdCameraManager.setMediaListener(new MediaStreamListener() {
            @Override
            public void getVideoStream(int i, byte[] bytes, int i1, int i2) {
                print("Stream");
                if (!photoTaken) {
                    sendPhoto(bytes);
                    photoTaken = true;
                    waitForPhoto = false;
                }
                hdCameraManager.closeStream(i);
            }

            @Override
            public void getAudioStream(int i, byte[] bytes) {

            }
        });
        hdCameraManager.openStream(streamOption);
    }

    public void sendPhotoStream() {
        configureStream();
    }

    private void configureStream() {
        sendPhotoRequest();
        StreamOption streamOption = new StreamOption();
        streamOption.setDecodType(StreamOption.HARDWARE_DECODE);
        streamOption.setChannel(StreamOption.MAIN_STREAM);
        streamOption.setJustIframe(true);
        hdCameraManager.setMediaListener(new MediaStreamListener() {
            @Override
            public void getVideoStream(int i, byte[] bytes, int i1, int i2) {
                sendPhoto(bytes);

                if(stopEmotion){
                    hdCameraManager.closeStream(i);
                    close();
                }
            }

            @Override
            public void getAudioStream(int i, byte[] bytes) {

            }
        });
        hdCameraManager.openStream(streamOption);
    }

    private void close(){
        try {
//            dataInputStream.close();
//            dataOutputStream.close();
//            socket.close();
            dataOutputStream.writeInt(-1);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void sendPhotoRequest(){
        try {
            dataInputStream = new DataInputStream(socket.getInputStream());
            dataOutputStream = new DataOutputStream(socket.getOutputStream());
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("service", ServiceID.emotionDetection);
            jsonObject.put("client", "");
            dataOutputStream.writeUTF(jsonObject.toString());
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void stopEmotion(){
        stopEmotion = true;
    }
}
