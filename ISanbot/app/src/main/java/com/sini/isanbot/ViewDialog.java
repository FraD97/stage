package com.sini.isanbot;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

public class ViewDialog extends Dialog implements
        android.view.View.OnClickListener {

    private String[] emotions;
    private String gender;
    private String ethnicity;
    private String age;
    private String propabilityAge;
    private String probabilityEthnicity;
    public Activity c;
    public Dialog d;

    public TextView textViewAge, textViewgender, textViewethinicity, textViewemotions, yes, no;

    public ViewDialog(Activity a) {
        super(a);
        this.c = a;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog);
        yes = findViewById(R.id.btn_yes);
        no = findViewById(R.id.btn_no);
        yes.setOnClickListener(this);
        no.setOnClickListener(this);

        textViewAge = findViewById(R.id.ageTextView);
        textViewgender = findViewById(R.id.genderTextView);
        textViewethinicity = findViewById(R.id.ethnicityTextView);
        textViewemotions = findViewById(R.id.emotionsTextView);

        configureTextViews();

    }

    @Override
    public void onClick(View v) {
        Log.d("Shish", "CLICK");
        dismiss();
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getEthnicity() {
        return ethnicity;
    }

    public void setEthnicity(String ethnicity) {
        this.ethnicity = ethnicity;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String[] getEmotions() {
        return emotions;
    }

    public void setEmotions(String[] emotions) {
        this.emotions = emotions;
    }

    public void setPropabilityAge(String propabilityAge) {
        this.propabilityAge = propabilityAge;
    }


    public void setProbabilityEthnicity(String probabilityEthnicity) {
        this.probabilityEthnicity = probabilityEthnicity;
    }

    private void configureTextViews(){
        textViewAge.setText(age + "\t(" + propabilityAge + "%)");
        textViewgender.setText(gender + " (100%)");
        textViewethinicity.setText(ethnicity + " (" + probabilityEthnicity +"%)");
        String emotionText = emotions[0] + " (più intensa), " + emotions[1] + ", " + emotions[2] + " (meno intensa)";
        textViewemotions.setText(emotionText);
    }

}