package com.sini.isanbot.Task;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;

import com.sanbot.opensdk.function.beans.LED;
import com.sanbot.opensdk.function.unit.HDCameraManager;
import com.sanbot.opensdk.function.unit.HeadMotionManager;
import com.sanbot.opensdk.function.unit.SpeechManager;
import com.sini.isanbot.ConversationActivity;
import com.sini.isanbot.Helper.MessageSender;
import com.sini.isanbot.MainActivity;
import com.sini.isanbot.Manager.HeadManager;
import com.sini.isanbot.Helper.PhotoSender;
import com.sini.isanbot.Helper.VideoSender;
import com.sini.isanbot.Manager.DialogueManager;
import com.sini.isanbot.Manager.InteractionManager;
import com.sini.isanbot.Manager.SanbotManager;
import com.sini.isanbot.Model.MessageAction;
import com.sini.isanbot.Model.ServerOption;
import com.sini.isanbot.Model.ServiceID;
import com.sini.isanbot.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

import static com.sini.isanbot.Model.MessageAction.CAN_LISTEN;
import static com.sini.isanbot.Model.MessageAction.LISTENING_ENDED;
import static com.sini.isanbot.Model.MessageAction.LISTENING_STARTED;
import static com.sini.isanbot.Model.MessageAction.PHOTO_TAKEN;
import static com.sini.isanbot.Model.MessageAction.SHOOT_PHOTO;
import static com.sini.isanbot.Model.MessageAction.SIGNAL_PROBLEM;
import static com.sini.isanbot.Model.MessageAction.TALKING;
import static com.sini.isanbot.Model.MessageAction.THINKING;
import static com.sini.isanbot.Model.MessageAction.UPDATE_SANBOT_TEXT;
import static com.sini.isanbot.Model.MessageAction.UPDATE_USER_TEXT;
import static com.sini.isanbot.Model.Utils.print;

/* This class handles the conversation activity. Note that it's a runnable, so you have to
 * create a thread in the conversation activity and start it.
 *
 */
public class ConversationTask implements Runnable {

    private SpeechManager speechManager;
    private HeadMotionManager headMotionManager;
    private HDCameraManager hdCameraManager;
    private Speaker speaker;
    private HeadManager headManager;
    private DialogueManager dialogueManager;
    private Socket socket;
    private DataInputStream dataInputStream;
    private DataOutputStream dataOutputStream;

    private Context context;
    private Boolean running = true;
    private MessageSender messageSender;

    private String clientName = "";
    private int clientID = 0;
    private String question = null;
    private String response = null;

    private Handler taskHandler;
    private boolean waitForUser;
    private InteractionManager interactionManager;

    private String color = "default";

    int interaction = 0;


    public ConversationTask(SanbotManager sanbotManager, Handler handler, ConversationActivity ca) {
        this.speechManager = sanbotManager.getSpeechManager();
        this.hdCameraManager = sanbotManager.getHdCameraManager();
        this.headMotionManager = sanbotManager.getHeadMotionManager();
        this.headManager = new HeadManager(headMotionManager);
        this.speaker = new Speaker(speechManager);
        this.messageSender = MessageSender.getInstance();
        createTaskHandler();
        ca.setTaskHandler(taskHandler);
        messageSender.setHandler(handler);
    }

    public void setUsername(String clientName) {
        this.clientName = clientName;
    }

    public void setClientID(int clientID) {
        this.clientID = clientID;
    }

    @Override
    public void run() {
        try {
            createSocket();

            //inizio foto
            messageSender.sendMessage(MessageAction.SHOOT_PHOTO);
            headManager.moveHeadUp();

            JSONObject jsonObject = new JSONObject();
            jsonObject.put("service", ServiceID.conversation);
            jsonObject.put("client", clientName);
            jsonObject.put("clientID", clientID);
            dataOutputStream.writeUTF(jsonObject.toString());

            startRecognition();

            startDialogue();

        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void createSocket() throws IOException {
        ServerOption serverOption = ServerOption.getInstance();
        socket = new Socket(serverOption.getHost(), serverOption.getPort());
        dataInputStream = new DataInputStream(socket.getInputStream());
        dataOutputStream = new DataOutputStream(socket.getOutputStream());
    }

    private void introduceYourSelf() {
        messageSender.sendCustomMessage(MessageAction.UPDATE_SANBOT_TEXT, "Ciao sono Sanbot!");
        speaker.speak("Ciao sono sanbot");
    }

    private void startRecognition() {
        introduceYourSelf();
        PhotoSender photoSender = new PhotoSender(socket, hdCameraManager);
        print("ora scatto una foto");
        messageSender.sendMessage(MessageAction.PHOTO_TAKEN);
        photoSender.takeAndSendPhoto();

    }

    private void startDialogue() {
        handleHandshake();
        while (running) {
            interaction++;
            messageSender.sendMessage(MessageAction.THINKING);
            if (dialogueManager.waitForNextQuestion()) {
                break; //no more questions;
            }
            dialogueManager.waitFromUserInput();
        }

        handleColor(color);

        messageSender.sendCustomMessage(MessageAction.UPDATE_SANBOT_TEXT, "E' stato un piacere, alla prossima!");
        speaker.speak("E' stato un piacere, alla prossima");
        Intent intent = new Intent(context, MainActivity.class);
        context.startActivity(intent);
        headManager.moveHeadDown();
        Thread.currentThread().interrupt();
    }

    private void handleHandshake() {
        messageSender.sendMessage(MessageAction.TALKING);
        messageSender.sendMessage(MessageAction.THINKING);
        String result = readResponseFromServer();
        messageSender.sendCustomMessage(MessageAction.UPDATE_SANBOT_TEXT, result);
        messageSender.sendMessage(MessageAction.TALKING);
        speaker.speak(result);

        dialogueManager = new DialogueManager(socket, speaker);
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public void setRunning(boolean running) {
        this.running = running;
    }

    private String readResponseFromServer() {
        question = null;
        int responseCode = 0;
        try {
            String jsonString = dataInputStream.readUTF();
            JSONObject jsonObject = new JSONObject(jsonString);
            question = jsonObject.getString("question");
            response = jsonObject.getString("response");
            responseCode = jsonObject.getInt("responseCode");

            setColor(jsonObject);


            print(jsonString);
            if (responseCode == ServiceID.failure) {
                print("FOTO SENZA VOLTO");
                while (recognitionFailed()) ;
            }
            print("VOLTO RILEVATO");


            if (!response.equals("")) {
                messageSender.sendCustomMessage(MessageAction.UPDATE_USER_TEXT, response);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return question;
    }

    public boolean recognitionFailed() {
        String jsonString = null;
        waitForUser = true;
        try {
            messageSender.sendCustomMessage(UPDATE_SANBOT_TEXT, question);
            messageSender.sendMessage(SIGNAL_PROBLEM);
            speaker.speak(question);

            //PREPARAZIONE PER SCATTO FOTO

            while (waitForUser) ;
            //CODICE PER SCATTARE LA FOTO

            messageSender.sendMessage(THINKING);

            jsonString = dataInputStream.readUTF();
            JSONObject jsonObject = new JSONObject(jsonString);
            if (jsonObject.getInt("responseCode") == ServiceID.failure) {
                return true;
            } else {
                question = jsonObject.getString("question");
                response = jsonObject.getString("response");
                setColor(jsonObject);
                return false;
            }

        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return false;
    }

    private void createTaskHandler() {
        taskHandler = new Handler(Looper.getMainLooper()) {
            @Override
            public void handleMessage(Message msg) {
                switch (msg.what) {
                    case MessageAction.NEW_PHOTO:
                        shootNewPhoto();
                        break;
                }
            }

            private void shootNewPhoto() {
//                headManager.moveHeadUp();
                PhotoSender photoSender = new PhotoSender(socket, hdCameraManager);
                photoSender.takeAndSendPhoto();
                waitForUser = false;
                messageSender.sendMessage(PHOTO_TAKEN);
//                headManager.moveHeadDown();
            }
        };
    }

    private void handleColor(String color) {
        if (interaction == 1) {
            if (interactionManager.setLedColor(color)) {
                speaker.speak("Hai visto che bravo che sono?");
            } else {
                interactionManager.setLedColor(new LED(LED.PART_ALL, LED.MODE_PINK));
                speaker.speak("Mi spiace ma non ho quel colore. Che ne dici del rosa?");
            }
        }
    }

    public void setInteractionManager(InteractionManager interactionManager) {
        this.interactionManager = interactionManager;
    }

    private void setColor(JSONObject jsonObject) {
        try {
            color = jsonObject.getString("color");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

}
