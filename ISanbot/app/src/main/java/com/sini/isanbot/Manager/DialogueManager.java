package com.sini.isanbot.Manager;

import com.sini.isanbot.Helper.MessageSender;
import com.sini.isanbot.Model.MessageAction;
import com.sini.isanbot.Task.Speaker;
import com.sini.isanbot.Helper.VoiceDetector;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

import static com.sini.isanbot.Model.Utils.print;

public class DialogueManager {

    private Speaker speaker;
    private AudioManager audioManager;
    private VoiceDetector voiceDetector;

    private Socket socket;
    private DataOutputStream dataOutputStream;
    private DataInputStream dataInputStream;
    private MessageSender messageSender = MessageSender.getInstance();
    private String color = "default";

    public DialogueManager(Socket socket, Speaker speaker) {
        this.socket = socket;
        this.speaker = speaker;
        this.audioManager = new AudioManager();
        this.voiceDetector = new VoiceDetector(audioManager);
        try {
            dataOutputStream = new DataOutputStream(socket.getOutputStream());
            dataInputStream = new DataInputStream(socket.getInputStream());
        } catch (IOException e) {
            e.printStackTrace();
            print("Errore input/output stream");
        }
    }

    public boolean waitForNextQuestion() {
        print("WAIT FROM SERVER...");
        String question = readResponseFromServer();
        print(question);
        messageSender.sendMessage(MessageAction.TALKING);
        if (question.equals("_end_")) {
            return true;
        }
        messageSender.sendCustomMessage(MessageAction.UPDATE_SANBOT_TEXT, question);
        speaker.speak(question);
        return false;
    }

    public void waitFromUserInput() {
        voiceDetector.run();
        audioManager.sendToServer(socket, voiceDetector.getCut());
    }


    private String readResponseFromServer() {
        String question = null;
        try {
            String jsonString = dataInputStream.readUTF();
            JSONObject jsonObject = new JSONObject(jsonString);
            question = jsonObject.getString("question");
            String response = jsonObject.getString("response");
            print(jsonString);
            if (!response.equals("")) {
                messageSender.sendCustomMessage(MessageAction.UPDATE_USER_TEXT, response);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return question;
    }

}
