package com.sini.isanbot.Manager;

import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.os.Environment;
import android.os.Handler;
import android.util.Log;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.Socket;
import java.util.UUID;

import static com.sini.isanbot.Model.Utils.print;

public class AudioManager {
    private static String TAG = "AudioManager";
    private MediaRecorder recorder;
    private MediaPlayer player;
    private String audioPath;

    public AudioManager() {
        initialSetup();
    }

    // start to register audio to audioPath
    public void startRegister() {
        try {
            recorder.prepare();
            recorder.start();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void recordAudio(int milliseconds) {
        try {
            print("Audio recording startato!");
            recorder.prepare();
            recorder.start();
            Thread.sleep(milliseconds);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        print("Audio finito");
        stopRegister();
    }

    public void stopRegister() {
        recorder.stop();
        recorder.release();
    }

    public double getMicrophoneValues() {
        return recorder.getMaxAmplitude();
    }

    //play the audio in audioPAth ( last recorded )
    public void playLastAudio() {
        prepareToPlay(this.audioPath);
        player.start();
    }

    public void play(String audioPath) {
        prepareToPlay(audioPath);
        player.start();
    }

    public void stopPlay() {
        if (player != null) {
            player.stop();
            player.release();
        }
    }

    public String getPath() {
        return this.audioPath;
    }

    //change the path where to save the audio
    public void setSavePath(String path) {
        this.audioPath = path;
        this.recorder.setOutputFile(path);
    }

    //get the AudioBytes of audio file specified in audioPath
    public byte[] getAudioBytes() {
        File file = new File(this.audioPath);
        //init array with file length
        if (file.exists()) {
            byte[] bytesArray = new byte[(int) file.length()];

            FileInputStream fis = null;
            try {
                fis = new FileInputStream(file);
                fis.read(bytesArray); //read file into bytes[]
                fis.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
                return null;
            } catch (IOException e) {
                e.printStackTrace();
                return null;
            }
            return bytesArray;
        } else {
            return null;
        }

    }

    public void sendToServer(Socket s) {
        File file = new File(audioPath);
        if (file.exists()) {
            try {
                byte[] audioByte = getAudioBytes();
                if (audioByte != null) {
                    DataOutputStream dOut = new DataOutputStream(s.getOutputStream());
                    dOut.writeInt(audioByte.length);
                    dOut.write(audioByte);
                } else {
                    Log.d(TAG, "getAudioBytes ha restituito null");
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

            Log.d(TAG, "cerco di eliminare il file " + file.getAbsolutePath());

            if (file.delete()) {
                Log.d(TAG, "eliminato correttamente");
            }
        } else {
            Log.d(TAG, "file da mandare non trovato");
        }
        initialSetup();
    }

    public void sendToServer(Socket s, int audioStartedAt) {
        File file = new File(audioPath);
        if (file.exists()) {
            try {
                byte[] audioByte = getAudioBytes();
                if (audioByte != null) {
                    DataOutputStream dOut = new DataOutputStream(s.getOutputStream());
                    dOut.writeInt(audioByte.length);
                    dOut.write(audioByte);
                    dOut.writeInt(audioStartedAt);
                } else {
                    Log.d(TAG, "getAudioBytes ha restituito null");
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

            print("Cerco di eliminare il file");

            if (file.delete()) {
                print("File eliminato correttamente");
            }
        } else {
            Log.d(TAG, "file da mandare non trovato");
        }
        initialSetup();
    }


    private void initialSetup() {
        recorder = new MediaRecorder();
        recorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        recorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
        recorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
        this.audioPath = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_MUSIC) + "/"
                + UUID.randomUUID().toString() + "_audio_record.3gp";

        recorder.setOutputFile(this.audioPath);

    }

    private void prepareToPlay(String path) {
        player = new MediaPlayer();
        try {
            player.setDataSource(path);
            player.prepare();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
