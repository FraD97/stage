package com.sini.isanbot.Manager;

import com.sanbot.opensdk.function.beans.EmotionsType;
import com.sanbot.opensdk.function.beans.LED;
import com.sanbot.opensdk.function.beans.handmotion.AbsoluteAngleHandMotion;
import com.sanbot.opensdk.function.beans.handmotion.NoAngleHandMotion;
import com.sanbot.opensdk.function.beans.handmotion.RelativeAngleHandMotion;
import com.sanbot.opensdk.function.beans.headmotion.RelativeAngleHeadMotion;
import com.sanbot.opensdk.function.unit.HandMotionManager;
import com.sanbot.opensdk.function.unit.HardWareManager;
import com.sanbot.opensdk.function.unit.HeadMotionManager;
import com.sanbot.opensdk.function.unit.SystemManager;
import com.sanbot.opensdk.function.beans.LED;

import static com.sini.isanbot.Model.Utils.print;

public class InteractionManager {

    private SystemManager systemManager;
    private HardWareManager hardWareManager;
    private HandMotionManager handMotionManager;
    private HeadMotionManager headMotionManager;


    public InteractionManager(SystemManager systemManager, HardWareManager hardWareManager, HandMotionManager handMotionManager, HeadMotionManager headMotionManager) {
        this.systemManager = systemManager;
        this.hardWareManager = hardWareManager;
        this.handMotionManager = handMotionManager;
        this.headMotionManager = headMotionManager;
    }

    public InteractionManager(SystemManager systemManager, HardWareManager hardWareManager, HandMotionManager handMotionManager) {
        this.systemManager = systemManager;
        this.hardWareManager = hardWareManager;
        this.handMotionManager = handMotionManager;
    }

    public void setEmotion(EmotionsType emotion) {
        systemManager.showEmotion(emotion);
    }

    public void setLedColor(LED led) {
        hardWareManager.setLED(led);
    }

    public void moveArm(RelativeAngleHandMotion relativeAngleHandMotion) {
        Thread thread = new Thread(new HandManager(relativeAngleHandMotion, handMotionManager));
        thread.start();
    }

    public boolean setLedColor(String color){
        if(color.contains("erde")){
            hardWareManager.setLED(new LED(LED.PART_ALL, LED.MODE_GREEN));
            return true;
        }
        else if(color.contains("osso")){
            hardWareManager.setLED(new LED(LED.PART_ALL, LED.MODE_RED));
            return true;
        }

        else if(color.contains("blu")){
            hardWareManager.setLED(new LED(LED.PART_ALL, LED.MODE_BLUE));
            return true;
        }
        else if(color.contains("osa")){
            hardWareManager.setLED(new LED(LED.PART_ALL, LED.MODE_PINK));
            return true;
        }
        else if(color.contains("iola")){
            hardWareManager.setLED(new LED(LED.PART_ALL, LED.MODE_PURPLE));
            return true;
        }
        else if(color.contains("anco")){
            hardWareManager.setLED(new LED(LED.PART_ALL, LED.MODE_WHITE));
            return true;
        }
        else if(color.contains("allo")){
            hardWareManager.setLED(new LED(LED.PART_ALL, LED.MODE_YELLOW));
            return true;
        }
        return false;
    }

    static class HandManager implements Runnable {

        private HandMotionManager handMotionManager;
        private RelativeAngleHandMotion relativeAngleHandMotion;

        public HandManager(RelativeAngleHandMotion relativeAngleHandMotion, HandMotionManager handMotionManager) {
            this.relativeAngleHandMotion = relativeAngleHandMotion;
            this.handMotionManager = handMotionManager;
        }

        @Override
        public void run() {
            handMotionManager.doRelativeAngleMotion(relativeAngleHandMotion);
            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            RelativeAngleHandMotion relativeAngleHandMotion = new RelativeAngleHandMotion( RelativeAngleHandMotion.PART_LEFT,5,
                    RelativeAngleHandMotion.ACTION_DOWN,
                    90
            );
            handMotionManager.doRelativeAngleMotion(relativeAngleHandMotion);
        }

    }
}
