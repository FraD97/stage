package com.sini.isanbot.Manager;

import com.sanbot.opensdk.function.beans.headmotion.LocateAbsoluteAngleHeadMotion;
import com.sanbot.opensdk.function.unit.HeadMotionManager;

public class HeadManager {

    private HeadMotionManager headMotionManager;

    public HeadManager(HeadMotionManager headMotionManager) {
        this.headMotionManager = headMotionManager;
    }

    private void moveHead(int direction) {
        LocateAbsoluteAngleHeadMotion locateAbsoluteAngleHeadMotion = null;
        switch (direction) {
            case 0:
                locateAbsoluteAngleHeadMotion = new
                        LocateAbsoluteAngleHeadMotion(
                        LocateAbsoluteAngleHeadMotion.ACTION_BOTH_LOCK, 90, 30);
                break;
            case 1:
                locateAbsoluteAngleHeadMotion = new
                        LocateAbsoluteAngleHeadMotion(
                        LocateAbsoluteAngleHeadMotion.ACTION_BOTH_LOCK, 90, 15);
                break;
        }
        headMotionManager.doAbsoluteLocateMotion(locateAbsoluteAngleHeadMotion);
    }

    public void moveHeadUp() {
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                moveHead(0);
                try {
                    Thread.sleep(1200);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
        t.start();
    }

    public void moveHeadDown() {

        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                moveHead(1);
            }
        });
        t.start();
    }
}
