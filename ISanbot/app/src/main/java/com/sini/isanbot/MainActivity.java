package com.sini.isanbot;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.sanbot.opensdk.base.BindBaseActivity;
import com.sanbot.opensdk.beans.FuncConstant;
import com.sanbot.opensdk.function.beans.EmotionsType;
import com.sanbot.opensdk.function.beans.LED;
import com.sanbot.opensdk.function.beans.handmotion.NoAngleHandMotion;
import com.sanbot.opensdk.function.beans.handmotion.RelativeAngleHandMotion;
import com.sanbot.opensdk.function.beans.headmotion.AbsoluteAngleHeadMotion;
import com.sanbot.opensdk.function.beans.headmotion.LocateAbsoluteAngleHeadMotion;
import com.sanbot.opensdk.function.unit.HandMotionManager;
import com.sanbot.opensdk.function.unit.HardWareManager;
import com.sanbot.opensdk.function.unit.HeadMotionManager;
import com.sanbot.opensdk.function.unit.SpeechManager;
import com.sanbot.opensdk.function.unit.SystemManager;
import com.sanbot.opensdk.function.unit.interfaces.hardware.TouchSensorListener;
import com.sini.isanbot.Manager.InteractionManager;
import com.sini.isanbot.Model.ServerOption;


import static com.sini.isanbot.Model.Utils.print;


public class MainActivity extends BindBaseActivity {

    private SystemManager systemManager;
    private HandMotionManager handMotionManager;
    private SpeechManager speechManager;
    private HardWareManager hardWareManager;
    private HeadMotionManager headMotionManager;
    private boolean alreadyTouched = false;
    private TextView textViewTouchMe;
    private CardView cardViewConversation;
    private CardView cardViewServices;
    private ImageView imageViewExit;


    private View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Intent intent = null;
            switch (view.getId()) {
                case R.id.cardviewConversation:
                    print("conversation");
//                    moveHead(0);
                    intent = new Intent(getApplicationContext(), ConversationActivity.class);
                    startActivity(intent);
                    finish();
                    break;
                case R.id.cardviewService:
                    intent = new Intent(getApplicationContext(), ServicesActivity.class);
                    startActivity(intent);
                    print("Services");
                    finish();
                    break;
                case R.id.touchMeTextview:
                    print("head");
                    break;
                case R.id.imageViewExit:
                    finish();
                    System.exit(0);
                    print("exit");
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        register(MainActivity.class);
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON, WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setContentView(R.layout.activity_main);

        checkPermissions();


        textViewTouchMe = findViewById(R.id.touchMeTextview);
        Animation anim = new AlphaAnimation(0.0f, 1.5f);
        anim.setDuration(800); //You can manage the blinking time with this parameter
        anim.setStartOffset(0);
        anim.setRepeatMode(Animation.REVERSE);
        anim.setRepeatCount(Animation.INFINITE);
        textViewTouchMe.startAnimation(anim);

        cardViewConversation = findViewById(R.id.cardviewConversation);
        cardViewServices = findViewById(R.id.cardviewService);
        imageViewExit = findViewById(R.id.imageViewExit);

        cardViewConversation.setOnClickListener(onClickListener);
        cardViewServices.setOnClickListener(onClickListener);
        textViewTouchMe.setOnClickListener(onClickListener);
        imageViewExit.setOnClickListener(onClickListener);


    }


    @Override
    protected void onMainServiceConnected() {
        initManager();
        sayHello();

    }

    @Override
    protected void onPause() {
        finish();
        super.onPause();
    }

    private void sayHello(){
        InteractionManager interactionManager = new InteractionManager(systemManager, hardWareManager, handMotionManager);
        RelativeAngleHandMotion relativeAngleHandMotion = new RelativeAngleHandMotion( RelativeAngleHandMotion.PART_LEFT,5,
                RelativeAngleHandMotion.ACTION_UP,
                90
        );
        interactionManager.moveArm(relativeAngleHandMotion);
        interactionManager.setEmotion(EmotionsType.SMILE);
        interactionManager.setLedColor(new LED(LED.PART_ALL, LED.MODE_BLUE));
        speechManager.startSpeak("Benvenuto, toccami la testa per iniziare oppure clicca su attività. ");


    }
    private void initManager() {
        handMotionManager = (HandMotionManager) getUnitManager(FuncConstant.HANDMOTION_MANAGER);
        systemManager = (SystemManager) getUnitManager(FuncConstant.SYSTEM_MANAGER);
        hardWareManager = (HardWareManager) getUnitManager(FuncConstant.HARDWARE_MANAGER);
        headMotionManager = (HeadMotionManager) getUnitManager(FuncConstant.HEADMOTION_MANAGER);
        speechManager = (SpeechManager) getUnitManager(FuncConstant.SPEECH_MANAGER);

        hardWareManager = (HardWareManager) getUnitManager(FuncConstant.HARDWARE_MANAGER);
        hardWareManager.setOnHareWareListener(new TouchSensorListener() {
            @Override
            public void onTouch(int i) {
                if ((i == 11 || i == 12 || i == 13) && !alreadyTouched) { //Head Triggered
                    alreadyTouched = true;
                    print("Testa Toccata");
                    Intent intent = new Intent(getBaseContext(), ConversationActivity.class);
                    startActivity(intent);
                    finish();
                }
            }

            @Override
            public void onTouch(int i, boolean b) {

            }
        });
    }


    public void moveHead(int direction) {
        LocateAbsoluteAngleHeadMotion locateAbsoluteAngleHeadMotion = null;
        switch (direction) {
            case 0:
                locateAbsoluteAngleHeadMotion = new
                        LocateAbsoluteAngleHeadMotion(
                        LocateAbsoluteAngleHeadMotion.ACTION_NO_LOCK, 90, 30);
                break;
            case 1:
                locateAbsoluteAngleHeadMotion = new
                        LocateAbsoluteAngleHeadMotion(
                        LocateAbsoluteAngleHeadMotion.ACTION_NO_LOCK, 90, 15);
                break;
        }
        headMotionManager.doAbsoluteLocateMotion(locateAbsoluteAngleHeadMotion);
    }

    private void checkPermissions() {
        if (ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.RECORD_AUDIO) == PackageManager.PERMISSION_GRANTED) {
        }
        // Else ask for permission
        else {
            ActivityCompat.requestPermissions(MainActivity.this, new String[]
                    {Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.RECORD_AUDIO}, 0);

        }
    }
}
