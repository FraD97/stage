import face_recognition
import argparse
import os

parser = argparse.ArgumentParser(description='Path of image to check')
parser.add_argument('unknown', type=str, help='Path of images')
parser.add_argument('known', type=str, help='Path of images')
args = parser.parse_args()

features = []

unknown_image = face_recognition.load_image_file(args.unknown)
try:
    unknown_encoding = face_recognition.face_encodings(unknown_image)[0]
except:
    quit(-1)


with open(args.unknown.rstrip("jpg") + "txt",'w') as f:                
    for x in unknown_encoding:
        f.write(str(x))
        f.write('\n')
        

for file in os.listdir(args.known):
    print(args.known + file)
    f = open(args.known +  file, "r")
    for x in f:
      features.append(float(x.rstrip("\n")))
  
    result = face_recognition.compare_faces([features], unknown_encoding)
    if(result[0]):
        id = os.path.splitext(file)[0]
        quit(int(id))
        
    features.clear()

quit(0)