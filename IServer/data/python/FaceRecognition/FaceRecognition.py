import face_recognition
import argparse
import os

parser = argparse.ArgumentParser(description='Path of image to check')
parser.add_argument('unknown', type=str, help='Path of images')
parser.add_argument('known', type=str, help='Path of images')
args = parser.parse_args()



unknown_image = face_recognition.load_image_file(args.unknown)

try:
    unknown_encoding = face_recognition.face_encodings(unknown_image)[0]
    print(unknown_encoding)
except:
    print("problem with image " + args.unknown)
    quit(-1)

for file in os.listdir(args.known):
    print(file)
    known_image = face_recognition.load_image_file(args.known + file)
    try:
        biden_encoding = face_recognition.face_encodings(known_image)[0]
        result = face_recognition.compare_faces([biden_encoding], unknown_encoding)
        if(result[0]):
            print("trovato match: " + file)
            id = os.path.splitext(file)[0]
            quit(int(id))
    except:
        print("problem with image " + file)
        quit(-1)

quit(0)
