import face_recognition
import argparse
import os

parser = argparse.ArgumentParser(description='Path of image to check')
parser.add_argument('unknown', type=str, help='Path of images')
parser.add_argument('known', type=str, help='Path of images')
args = parser.parse_args()

features = []

unknown_image = face_recognition.load_image_file(args.unknown)
try:
    unknown_encoding = face_recognition.face_encodings(unknown_image, None, 6)
    if len(unknown_encoding) > 1:
        print(-2)
        quit(-2)
    else:
        unknown_encoding = unknown_encoding[0]
except Exception as e:
    print(-1)
    quit(-1)


with open(args.unknown.rstrip("jpg") + "txt",'w') as f:                
    for x in unknown_encoding:
        f.write(str(x))
        f.write('\n')
        

for user in os.listdir(args.known):
    userDir = args.known + "\\" + user + "\\"
    f = open(userDir + "features\\features.txt", "r")
    
    for x in f:     # x rappresenta il valore letto dal file.txt che andrà messo nel vettore features
        features.append(float(x.rstrip("\n")))
    
    result = face_recognition.compare_faces([features], unknown_encoding,0.54)
    
    if(result[0]):
        print(int(user))
        quit(int(user))
        
    features.clear()
    
print(0)        
quit(0)