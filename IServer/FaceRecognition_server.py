import face_recognition
import argparse
import os
import socket

def startRecognition(unknown,known,conn):
    
    features = []

    unknown_image = face_recognition.load_image_file(unknown)
    try:
        unknown_encoding = face_recognition.face_encodings(unknown_image, None, 1)
        if len(unknown_encoding) > 1:
            print("ho trovato 2 volti" + str(-2))
            conn.send(len(str(-2)).to_bytes(2, byteorder='big'))
            conn.send(str(-2).encode('utf-8'))
            return;
        else:   
            unknown_encoding = unknown_encoding[0]
    except Exception as e:
        print(-1)
        conn.send(len(str(-1)).to_bytes(2, byteorder='big'))
        conn.send(str(-1).encode('utf-8'))
        return;


    with open(unknown.rstrip("jpg") + "txt",'w') as f:                
        for x in unknown_encoding:
            f.write(str(x))
            f.write('\n')
            

    for user in os.listdir(known):
        userDir = known + "\\" + user + "\\"
        f = open(userDir + "features\\features.txt", "r")
        
        for x in f:     # x rappresenta il valore letto dal file.txt che andrà messo nel vettore features
            features.append(float(x.rstrip("\n")))
        
        result = face_recognition.compare_faces([features], unknown_encoding,0.54)
        
        if(result[0]):
            print(int(user))
            conn.send(len(user).to_bytes(2, byteorder='big'))
            conn.send(user.encode('utf-8'))
            return;
            
        features.clear()
        
    print(0)
    conn.send(len(str(0)).to_bytes(2, byteorder='big'))
    conn.send(str(0).encode('utf-8'))
    
    return;



HOST = '127.0.0.1'  # Standard loopback interface address (localhost)
PORT = 8686       # Port to listen on (non-privileged ports are > 1023)

with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
    s.bind((HOST, PORT))
    s.listen()
    while(True):
        conn, addr = s.accept()
        with conn:
            header = conn.recv(2)
            path = conn.recv(1024).decode('utf-8')
            paths = path.split("\n")
            startRecognition(paths[0],paths[1],conn)
            conn.close()