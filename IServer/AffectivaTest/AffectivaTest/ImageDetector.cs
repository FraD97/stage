﻿using Affdex;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Net;
using System.Net.Sockets;
using System.Runtime.InteropServices;
using System.Text;

namespace AffectivaTest
{
    class EmotionDetector : Affdex.ImageListener, FaceListener, ProcessStatusListener
    {
        PhotoDetector detector;
        TcpListener listener;
        TcpClient client;
        NetworkStream nwStream;

        static void Main()
        {
       
            EmotionDetector emotionDetector = new EmotionDetector();
            emotionDetector.createWelcomeSocket();
            while (true)
            {
                String path = emotionDetector.getFramePath();
                if (path == null)
                {
                    emotionDetector.closeClient();
                    break;
                }
                emotionDetector.processFrame(path);
            }
        }

        public EmotionDetector()
        {
            ConfigureDetector();
        }

        private void ConfigureDetector()
        {
            detector = new Affdex.PhotoDetector(1, (Affdex.FaceDetectorMode)1);
            String path = "C:\\Affectiva\\AffdexSDK\\data";
            detector.setClassifierPath(path);
            detector.setFaceListener(this);
            detector.setDetectAllExpressions(true);
            detector.setDetectAllEmotions(true);
            detector.setDetectAllEmojis(true);
            detector.setDetectAllAppearances(true);
            detector.start();

        }

        public void processFrame(String path)
        {
            Affdex.Frame frame = loadFrameFromFile(path);
            detector.process(frame);
        }

        public void stopDetection()
        {
            detector.stop();
            detector.reset();
        }

        public Affdex.Frame loadFrameFromFile(string fileName)
        {
            Bitmap bitmap = new Bitmap(fileName);

            // Lock the bitmap's bits.
            Rectangle rect = new Rectangle(0, 0, bitmap.Width, bitmap.Height);
            BitmapData bmpData = bitmap.LockBits(rect, ImageLockMode.ReadWrite, bitmap.PixelFormat);

            // Get the address of the first line.
            IntPtr ptr = bmpData.Scan0;

            // Declare an array to hold the bytes of the bitmap. 
            int numBytes = bitmap.Width * bitmap.Height * 3;
            byte[] rgbValues = new byte[numBytes];

            int data_x = 0;
            int ptr_x = 0;
            int row_bytes = bitmap.Width * 3;


            for (int y = 0; y < bitmap.Height; y++)
            {
                Marshal.Copy(ptr + ptr_x, rgbValues, data_x, row_bytes);//(pixels, data_x, ptr + ptr_x, row_bytes);
                data_x += row_bytes;
                ptr_x += bmpData.Stride;
            }

            bitmap.UnlockBits(bmpData);

            return new Affdex.Frame(bitmap.Width, bitmap.Height, rgbValues, Affdex.Frame.COLOR_FORMAT.BGR);
        }

        public void onImageResults(Dictionary<int, Face> faces, Frame frame)
        {
            throw new NotImplementedException();
        }

        public void onImageCapture(Frame frame)
        {
            throw new NotImplementedException();
        }

        public void onFaceFound(float timestamp, int faceId)
        {
            Console.WriteLine("VOLTO RILEVATO");
        }

        public void onFaceLost(float timestamp, int faceId)
        {
            Console.WriteLine("VOLTO NON RILEVATO");
        }

        public void onProcessingException(AffdexException ex)
        {
            throw new NotImplementedException();
        }

        public void onProcessingFinished()
        {
            throw new NotImplementedException();
        }

        private void createWelcomeSocket()
        {
            listener = new TcpListener(IPAddress.Any, 4444);
            Console.WriteLine("Listening...");
            listener.Start();
        }

        private String getFramePath()
        {
            try
            {
                if (client == null)
                {
                    Console.WriteLine("In attesa di connessione...");
                    client = listener.AcceptTcpClient();
                    //---get the incoming data through a network stream---
                    nwStream = client.GetStream();
                }

                byte[] buffer = new byte[client.ReceiveBufferSize];

                //---read incoming stream---
                int bytesRead = nwStream.Read(buffer, 0, client.ReceiveBufferSize);

                //---convert the data received into a string---
                String dataReceived = Encoding.UTF8.GetString(buffer, 2, bytesRead);
                Console.WriteLine("Received: " + dataReceived);
                return dataReceived;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        private void closeClient()
        {
            client.Close();
        }
    }
}
