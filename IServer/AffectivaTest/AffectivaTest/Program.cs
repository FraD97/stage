﻿using Affdex;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Runtime.InteropServices;

public class HelloWorld : Affdex.ImageListener, FaceListener, ProcessStatusListener
{

    //public static void Main()
    //{
    //    HelloWorld helloWorld = new HelloWorld();
    //    helloWorld.startImageDetection();
    //    Console.WriteLine("STOP");
    //    Console.ReadLine();
    //}


    public void startVideoDetection()
    {

        VideoDetector detector = new VideoDetector();
        String path = "C:\\Affectiva\\AffdexSDK\\data";
        detector.setClassifierPath(path);

        detector.setImageListener(this);
        detector.setProcessStatusListener(this);
        detector.setDetectAllEmotions(true);
        detector.setDetectAllExpressions(true);
        detector.setDetectAllEmojis(true);
        detector.setDetectAllAppearances(true);
        detector.setDetectAge(true);
        detector.setDetectEthnicity(true);
        detector.start();
        detector.process("C:\\Users\\fradi\\Desktop\\test\\test.mp4");
        detector.stop();
    }

    public void startImageDetection()
    {

        Affdex.PhotoDetector detector = new Affdex.PhotoDetector(1, (Affdex.FaceDetectorMode)1);

        String path = "C:\\Affectiva\\AffdexSDK\\data";
        detector.setClassifierPath(path);

        detector.setImageListener(this);
        detector.setFaceListener(this);


        detector.setDetectAllExpressions(true);
        detector.setDetectAllEmotions(true);
        detector.setDetectAllEmojis(true);
        detector.setDetectAge(true);
        detector.setDetectEthnicity(true);
        detector.setDetectAllAppearances(true);
        detector.start();

        int count = 1;
        while (count <= 20)
        {
            Console.WriteLine("Immagine numero " + count);
            Frame frame = loadFrameFromFile("C:\\Users\\fradi\\Desktop\\test\\thumb" + count + ".jpg");
            detector.process(frame);
            count++;
        }

        detector.stop();
    }

    public void onImageCapture(Frame frame)
    {
        frame.Dispose();
    }

    public void onImageResults(Dictionary<int, Face> faces, Frame frame)
    {

        foreach (KeyValuePair<int, Affdex.Face> pair in faces)
        {
            Affdex.Face face = pair.Value;
            Console.WriteLine(face.Appearance.Gender.ToString());
            Console.WriteLine(face.Appearance.Age.ToString());
            Console.WriteLine(face.Appearance.Ethnicity.ToString());
            Console.WriteLine("Rabbia: " + face.Emotions.Anger.ToString());
            Console.WriteLine("Gioia: " + face.Emotions.Joy.ToString());
            Console.WriteLine("Tristezza: " + face.Emotions.Sadness.ToString());
            Console.WriteLine("Sorpresa: " + face.Emotions.Surprise.ToString());
            Console.WriteLine("Paura: " + face.Emotions.Fear.ToString());
            Console.WriteLine("Disgusto: " + face.Emotions.Disgust.ToString());
            Console.WriteLine("Disprezzo: " + face.Emotions.Contempt.ToString());
            Console.WriteLine("Espressività: " + face.Emotions.Engagement.ToString());
            Console.WriteLine("Valenza: " + face.Emotions.Valence.ToString());
        }


        frame.Dispose();
    }


    static Affdex.Frame loadFrameFromFile(string fileName)
    {
        Bitmap bitmap = new Bitmap(fileName);

        // Lock the bitmap's bits.
        Rectangle rect = new Rectangle(0, 0, bitmap.Width, bitmap.Height);
        BitmapData bmpData = bitmap.LockBits(rect, ImageLockMode.ReadWrite, bitmap.PixelFormat);

        // Get the address of the first line.
        IntPtr ptr = bmpData.Scan0;

        // Declare an array to hold the bytes of the bitmap. 
        int numBytes = bitmap.Width * bitmap.Height * 3;
        byte[] rgbValues = new byte[numBytes];

        int data_x = 0;
        int ptr_x = 0;
        int row_bytes = bitmap.Width * 3;


        for (int y = 0; y < bitmap.Height; y++)
        {
            Marshal.Copy(ptr + ptr_x, rgbValues, data_x, row_bytes);//(pixels, data_x, ptr + ptr_x, row_bytes);
            data_x += row_bytes;
            ptr_x += bmpData.Stride;
        }

        bitmap.UnlockBits(bmpData);

        return new Affdex.Frame(bitmap.Width, bitmap.Height, rgbValues, Affdex.Frame.COLOR_FORMAT.BGR);
    }

    public void onFaceFound(float timestamp, int faceId)
    {
        Console.WriteLine("VOLTO RILEVATO");
    }

    public void onFaceLost(float timestamp, int faceId)
    {
        Console.WriteLine("VOLTO PERSO");
    }

    public void onProcessingException(AffdexException ex)
    {

    }

    public void onProcessingFinished()
    {
        Console.WriteLine("");
    }
}