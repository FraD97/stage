﻿using Affdex;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AffectivaTest
{
    public class Streaming /*: Affdex.ImageListener, FaceListener, ProcessStatusListener*/
    {
        //FrameDetector detector;
        /*
        static void Main()
        {
            Streaming streaming = new Streaming();
            streaming.configureDetector();
        }
        */

        private void configureDetector()
        {
            //detector = new FrameDetector(2);
            //String path = "C:\\Affectiva\\AffdexSDK\\data";
            //detector.setClassifierPath(path);
            //detector.setProcessStatusListener(this);
            //detector.setImageListener(this);
            //detector.setFaceListener(this);

            //detector.setDetectAllExpressions(true);
            //detector.setDetectAllEmotions(true);
            //detector.setDetectAllEmojis(true);
            //detector.setDetectAllAppearances(true);

            //detector.start();
            //Console.WriteLine("Detector startato");
            //Console.ReadLine();
            //Console.WriteLine("Frame creato");
            //Console.ReadLine();
        

            //Console.WriteLine("Frame processato");
            //Console.ReadLine();
            //detector.stop();
        }

        private void extractFrame()
        {
           
        }

        private void createFrame()
        {
            Image image = Image.FromFile("C:\\Users\\fradi\\Desktop\\test\\thumb0001.jpg");
            var memoryStream = new MemoryStream();
            image.Save(memoryStream, System.Drawing.Imaging.ImageFormat.Jpeg);
            var bytes = memoryStream.ToArray();

            var imageMemoryStream = new MemoryStream(bytes);
            Image imageFromStream = Image.FromStream(imageMemoryStream);
        }

        private void startDetector()
        {
            //detector.start();
        }

        private void stopDetector()
        {
            //detector.stop();
        }

        public void onFaceFound(float timestamp, int faceId)
        {
            throw new NotImplementedException();
        }

        public void onFaceLost(float timestamp, int faceId)
        {
            throw new NotImplementedException();
        }

        public void onImageCapture(Frame frame)
        {
            throw new NotImplementedException();
        }

        public void onImageResults(Dictionary<int, Face> faces, Frame frame)
        {
            foreach (KeyValuePair<int, Affdex.Face> pair in faces)
            {
                Affdex.Face face = pair.Value;
                Console.WriteLine(face.Appearance.Gender.ToString());
                Console.WriteLine(face.Appearance.Age.ToString());
                Console.WriteLine(face.Appearance.Ethnicity.ToString());
                Console.WriteLine("Rabbia: " + face.Emotions.Anger.ToString());
                Console.WriteLine("Gioia: " + face.Emotions.Joy.ToString());
                Console.WriteLine("Tristezza: " + face.Emotions.Sadness.ToString());
                Console.WriteLine("Sorpresa: " + face.Emotions.Surprise.ToString());
                Console.WriteLine("Paura: " + face.Emotions.Fear.ToString());
                Console.WriteLine("Disgusto: " + face.Emotions.Disgust.ToString());
                Console.WriteLine("Disprezzo: " + face.Emotions.Contempt.ToString());
                Console.WriteLine("Espressività: " + face.Emotions.Engagement.ToString());
                Console.WriteLine("Valenza: " + face.Emotions.Valence.ToString());
            }

            frame.Dispose();
            Console.ReadLine();
        }

        public void onProcessingException(AffdexException ex)
        {
            throw new NotImplementedException();
        }

        public void onProcessingFinished()
        {
            throw new NotImplementedException();
        }
    }
}
