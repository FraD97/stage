﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AffectivaTest
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void PictureBox1_Click(object sender, EventArgs e)
        {
           
        }

        public void createFrame(int count)
        {
            Console.WriteLine("Immagine: " + count);
            Image image = Image.FromFile("C:\\Users\\fradi\\Desktop\\test\\thumb" + count + ".jpg");
            var memoryStream = new MemoryStream();
            image.Save(memoryStream, System.Drawing.Imaging.ImageFormat.Jpeg);
            var bytes = memoryStream.ToArray();

            var imageMemoryStream = new MemoryStream(bytes);
            Image imageFromStream = Image.FromStream(imageMemoryStream);
            pictureBox1.Image = imageFromStream;
        }

        public void test()
        {
            Console.WriteLine("OK");
        }

        private void Form1_Load(object sender, EventArgs e)
        {
           
        }

        private void Form1_Shown(object sender, EventArgs e)
        {
            Console.WriteLine("FORM_CREATED");
            int count = 1;
            while (count <= 50)
            {
                
                createFrame(count);
                count++;
            }

        }
    }
}
