package model;

public class Question {

    private int id;
    private int attribute;
    private String text;
    private int confidentiality;
    private int tone;

    public Question() {}

    public Question(int id, int attribute, String text, int confidentiality, int tone) {
        this.id = id;
        this.attribute = attribute;
        this.text = text;
        this.confidentiality = confidentiality;
        this.tone = tone;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getAttribute() {
        return attribute;
    }

    public void setAttribute(int attribute) {
        this.attribute = attribute;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public int getConfidentiality() {
        return confidentiality;
    }

    public void setConfidentiality(int confidentiality) {
        this.confidentiality = confidentiality;
    }

    public int getTone() {
        return tone;
    }

    public void setTone(int tone) {
        this.tone = tone;
    }

    public String toString(){
        return "\nDomanda: " + text + "\nid: " + id + "\nattributo: " + attribute + "\ntono: " + tone
                + "\nconfidenza: " + confidentiality;
    }
}
