package model;

import java.util.ArrayList;
import java.util.List;

public class Person {

    private int id;
    private List<Attribute> attributeList;

    public Person(int id) {
        this.id = id;
        attributeList = new ArrayList<>();
    }

    public Person(int id, List<Attribute> attributeList){
        this.id = id;
        this.attributeList = new ArrayList<>(attributeList);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setAttributeList(List<Attribute> attributeList){
        this.attributeList.addAll(attributeList);
    }

    public List<Attribute> getAttributeList(){
        return attributeList;
    }

    public String toString(){
        return id+", "+ attributeList;
    }

}
