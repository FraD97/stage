package model;

import org.json.JSONException;
import org.json.JSONObject;

import java.awt.*;
import java.util.HashMap;
import java.util.Set;
import java.util.function.BiConsumer;

public class FrameData {


    //
    private JSONObject jsonObject;
    HashMap<String, Integer> emotions;
    HashMap<String, String> appearanceData;
    private String dominantEmotion;


    public FrameData(JSONObject jsonObject) {
        this.jsonObject = jsonObject;
        emotions = new HashMap<>();
        appearanceData = new HashMap<>();

        try {
            emotions.put("joy", jsonObject.getInt("joy"));
            emotions.put("anger", jsonObject.getInt("anger"));
            emotions.put("sadness", jsonObject.getInt("sadness"));
            emotions.put("disgust", jsonObject.getInt("disgust"));
            emotions.put("contempt", jsonObject.getInt("contempt"));
            emotions.put("surprise", jsonObject.getInt("surprise"));
            emotions.put("fear", jsonObject.getInt("fear"));
            appearanceData.put("age", jsonObject.getString("age"));
            appearanceData.put("gender", jsonObject.getString("gender"));
            appearanceData.put("ethnicity", jsonObject.getString("ethnicity"));
            Boolean b = jsonObject.getBoolean("faceDetected");
            appearanceData.put("faceDetected", b.toString());

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public String findDominantEmotion() {
        Set set = emotions.keySet();
        int max = 0;
        String emotion = null;
        for (Object s : set) {
            if (emotions.get(s) > max) {
               max = emotions.get(s);
               emotion = (String)s;
            }
        }
        dominantEmotion = emotion;
        try {
            if(max != 0){
                jsonObject.put("dominantEmotion", emotion);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return emotion;
    }

    public HashMap<String, Integer> getEmotions() {
        return emotions;
    }

    public HashMap<String, String> getAppearanceData() {
        return appearanceData;
    }

    public JSONObject getJsonObject() {
        return jsonObject;
    }

    public boolean faceDetected(){
        String result = appearanceData.get("faceDetected");
        return result.equals("true");
    }

}
