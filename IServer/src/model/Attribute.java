package model;

public class Attribute {
    private int id;
    private String name;
    private String description;
    private String value;


    public Attribute(int id, String value) {
        this.id = id;
        this.value = value;
    }

    public Attribute(int id, String name, String description, String value) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.value = value;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String toString(){
        return "(" + id + ", " + name + ", "+ value + ")";
    }

}
