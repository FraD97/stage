package subTasks;

import database.DatabaseHelper;
import helper.CmdLaunch;
import helper.JsonManager;
import helper.MediaReceiver;
import helper.Utils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.*;
import java.net.Socket;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;
import static java.lang.Thread.sleep;

public class FaceRecognizer {
    private Socket s;
    private String clientName;
    private int userID;
    private String userImage;
    public FaceRecognizer(Socket s, String clientName) {
        this.s = s;
        this.clientName = clientName;
    }

    public int startRecognition() {
        MediaReceiver mediaReceiver = new MediaReceiver(s);
        String imagePath = mediaReceiver.receiveImage(); // path dell'immagine ricevuta con estensione .jpg (tempImage)
        try {
            sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        int exitValue = CmdLaunch.startFaceRecognition(imagePath, Utils.clientDir + clientName); // effettuo il check delle features

        String withoutExtension = imagePath.substring(0, imagePath.lastIndexOf('.'));

        if (exitValue == 0) { // se exitValue è = a 0 vuol dire che l'utente non era mai stato incontrato
            userImage = updateUserDir(withoutExtension);
            insertNewPersonIntoDB();
            exitValue = this.userID;
        }

        //deleteTempImage(withoutExtension);

        return exitValue;
    }

    private String updateUserDir(String withoutExtension) {
        Path source = Paths.get(withoutExtension + ".txt");
        System.out.println("immagine ricevuta: " + withoutExtension);
        String id = getID();
        createUserDirectories(id);
        System.out.println("nuovo id: " + id);
        Path dest = Paths.get(Utils.clientDir + "\\" + this.clientName + "\\" + id + "\\features\\" + "features.txt"); //sposto quindi il file delle features nella cartella features
        System.out.println("sposto " + source.toString() + "nella directory -> " + dest.toString());
        String destImage = "";
        try {
            Files.move(source, dest);
            destImage = Utils.clientDir + "\\" + this.clientName + "\\" + this.userID + "\\images\\" + "image.jpg";
            Files.move(Paths.get(withoutExtension + ".jpg"), Paths.get(destImage));
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }

        return destImage;
    }

    //restituisce un nuovo ID da assegnare alla persona posta d'avanti al sanbot.
    private String getID() {
        File file = new File(Utils.idPath);
        String id = null;

        try {
            Scanner scanner = new Scanner(file);
            id = scanner.next();
            scanner.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();

        }
        String newId = String.valueOf(Integer.parseInt(id) + 1);
        this.userID = Integer.parseInt(newId);

        PrintWriter printWriter = null;
        try {
            printWriter = new PrintWriter(file);
            printWriter.write(newId);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } finally {
            printWriter.close();
        }

        return newId;
    }

    private void insertNewPersonIntoDB() {
        DatabaseHelper databaseHelper = new DatabaseHelper();
        databaseHelper.connect();
        //String gender = detectGenderFromAffectiva();
        databaseHelper.insertPerson(userID);
    }

    private void createUserDirectories(String userID) {
        File userDir = new File(Utils.clientDir + "\\" + this.clientName + "\\" + userID);
        File imagesDir = new File(Utils.clientDir + "\\" + this.clientName + "\\" + userID + "\\" + "images\\");
        File featuresDir = new File(Utils.clientDir + "\\" + this.clientName + "\\" + userID + "\\" + "features\\");

        System.out.println(" sto creando la directory -> " + userDir.getAbsolutePath());
        System.out.println(" sto creando la directory -> " + imagesDir.getAbsolutePath());
        System.out.println(" sto creando la directory -> " + featuresDir.getAbsolutePath());

        userDir.mkdir();
        imagesDir.mkdir();
        featuresDir.mkdir();
    }

    private void deleteTempImage(String withoutExtension) {
        File image = new File(withoutExtension + ".jpg");
        File features = new File(withoutExtension + ".txt");
        image.delete();
        features.delete();
    }

    private String detectGenderFromAffectiva(){
        String gender = "";
        try {
            int service = 0;
            Socket emotionSocket = new Socket("localhost",4444);
            DataOutputStream outputStream = new DataOutputStream(emotionSocket.getOutputStream());
            outputStream.write(service);
            System.out.println("chiedo ad affectiva di analizzare l'immagine: " + this.userImage);
            outputStream.writeUTF(userImage);
            System.out.println("MAndato");
            JSONObject jsonObject = JsonManager.getJsonFromAffectiva(emotionSocket);
            gender = jsonObject.getString("gender");
            System.out.println("la persona è di sesso: " + gender);
            emotionSocket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }catch(JSONException e){e.printStackTrace();}

        return gender;
    }
}
