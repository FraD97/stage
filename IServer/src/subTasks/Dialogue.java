package subTasks;

import database.DatabaseHelper;
import helper.MediaReceiver;
import helper.ServiceID;
import model.Person;
import model.Question;
import org.json.JSONException;
import org.json.JSONObject;


import java.io.*;
import java.net.Socket;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.util.List;

import static helper.ServiceID.SUCCESS;

public class Dialogue {

    private Socket socket;
    private int userID;
    private DatabaseHelper databaseHelper;
    private int confidence;
    private int tone;

    public Dialogue(Socket socket, int userID){
        this.socket = socket;
        this.userID = userID;
        databaseHelper = new DatabaseHelper();
        databaseHelper.connect();
    }

    public Dialogue(Socket socket, int userID, int confidence, int tone) {
        this.socket = socket;
        this.userID = userID;
        this.confidence = confidence;
        this.tone = tone;
        databaseHelper = new DatabaseHelper();
        databaseHelper.connect();
    }

    public void startDialogue() {
        Person p = databaseHelper.getPerson(userID);
        System.out.println(p.getAttributeList());
        String name = p.getAttributeList().get(0).getValue();

        if (name != null) {
            String color = p.getAttributeList().get(1).getValue();
            sendTextToClient("ciao " + name + ", bentornato! Mi ricordo che il tuo colore preferito è " + color, "","color", color);
        } else {
            sendTextToClient("ciao, non ti ho mai visto qui.", "", null, null);
        }

        List<Question> questions = databaseHelper.getUnfilledQuestions(p.getAttributeList());
        System.out.println(questions);

        String response = "";
        short i = 0;
        for (Question question : questions) {
            if (i == 1 && name == null) {
                sendTextToClient("ciao " + response + ", piacere di conoscerti, " + question.getText(), response, null, null);
            } else {
                sendTextToClient(question.getText(), response, null, null);
            }
            i++;
            response = waitForClientResponse();
            registerResponse(userID, question.getAttribute(), response); // aggiorna l'attributo dell'utente userID con la risposta data.
        }
        sendTextToClient("_end_", response, null, null);
    }

    /*
     * this method open a connection with Wit Api Speech by sending a request with the audio file and the key
     * we will collect a Buffereader object from InputStream and convert it to Json that will contain the parsed audio
     * Setting can be changed from our Github developer account on wit api
     * */
    private String getTextBySpeech(String path) throws IOException {
        String url = "https://api.wit.ai/speech";
        String key = "FDSJZQIS6M3CFVU34UE4ZN64YTKIUCY7"; // token del progetto settabile da wit.ai

        String param1 = "20170203";
        String param2 = "command";
        String charset = "UTF-8";

        // sto settando la query con il format corretto per la richiesta
        String query = String.format("v=%s", URLEncoder.encode(param1, charset));

        // url di wit + query format + open connection
        URLConnection connection = new URL(url + "?" + query).openConnection();
        connection.setRequestProperty("Authorization", "Bearer " + key);
        connection.setRequestProperty("Content-Type", "audio/wav");
        connection.setDoOutput(true);
        OutputStream outputStream = connection.getOutputStream();
        FileChannel fileChannel = new FileInputStream(path).getChannel();
        ByteBuffer byteBuffer = ByteBuffer.allocate(1024);

        while ((fileChannel.read(byteBuffer)) != -1) {
            byteBuffer.flip();
            byte[] b = new byte[byteBuffer.remaining()];
            byteBuffer.get(b);
            outputStream.write(b);
            byteBuffer.clear();
        }

        BufferedReader response = new BufferedReader(new InputStreamReader(connection.getInputStream()));

        StringBuilder sb = new StringBuilder();
        String line;
        while ((line = response.readLine()) != null) {
            sb.append(line);
        }

        String parsedText = "";
        try {
            JSONObject json = new JSONObject(sb.toString());
            parsedText = (String) json.get("_text");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return parsedText;
    }

    //IMPORTANTE: question sono le stringhe che vanno dal server al client, response sono le strnghe che arrivano dal client verso il server
    private void sendTextToClient(String question, String response, String attribute, String value) {
        DataOutputStream dataOutputStream = null;
        try {
            dataOutputStream = new DataOutputStream(socket.getOutputStream());
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("question", question);
            jsonObject.put("response", response);
            jsonObject.put("responseCode", SUCCESS);
            if(attribute != null){
                jsonObject.put(attribute, value);
            }
            System.out.println("sto inviando l'oggetto json: " + jsonObject);
            dataOutputStream.writeUTF(jsonObject.toString());
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    //ricevo  l'audio dal sanbot che verrà poi interpretato dal servizio wit.
    private String waitForClientResponse() {
        MediaReceiver mediaReceiver = new MediaReceiver(socket);
        String audioPath = mediaReceiver.receiveAudioAndCut();
        String response = "";
        try {
            response = getTextBySpeech(audioPath);

        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println(response);
        return response;

    }

    private void registerResponse(int id, int attribute, String response) {
        databaseHelper.registerAttributeValue(id, attribute, response);
    }
}
