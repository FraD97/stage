import database.DatabaseHelper;
import helper.JsonManager;
import helper.Utils;
import mainTasks.*;
import model.Attribute;
import model.Person;
import model.Question;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

import static helper.ServiceID.*;

public class Main {

    public static void main(String[] args) {
        startDirectories();
        try {
            ServerSocket welcome = new ServerSocket(8189);
            Socket s = null;
            Thread t = null;
            int service = -1;
            String clientName = "";

            while (true) {
                s = welcome.accept();
                JSONObject fromClient = JsonManager.getJsonFromClient(s);
                System.out.println(fromClient);
                try {
                    service = fromClient.getInt("service");
                    clientName = fromClient.getString("client");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                System.out.println("richiesta servizio " + service + " da parte dell'utente " + clientName);
                switch (service) {
                    case LOGIN:
                        t = new Thread(new Login(s, clientName));
                        t.start();
                        break;

                    case CONVERSATION:
                        t = new Thread(new ConversationTask(s, clientName));
                        t.start();
                        break;

                    case PHOTO:
                        t = new Thread(new PhotoTask(s));
                        t.start();
                        break;

                    case VIDEO:
                        t = new Thread(new VideoTask(s));
                        t.start();
                        break;

                    case EMOTION_DETECTION:
                        t = new Thread(new EmotionTask(s));
                        t.start();
                        break;
                }

            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private static void startDirectories() {
        File file = new File(Utils.idPath);
        if (!file.exists()) {
            PrintWriter printWriter = null;
            try {
                printWriter = new PrintWriter(file);
                printWriter.write(String.valueOf(0));

                ArrayList<String> directoryList = Utils.getDirectoryList();
                for (String directory : directoryList) {
                    File dir = new File(directory);
                    dir.mkdir();
                }
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } finally {
                printWriter.close();
            }
        }
    }


}




