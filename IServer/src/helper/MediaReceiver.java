package helper;

import java.io.DataInputStream;
import java.io.EOFException;
import java.io.IOException;
import java.net.Socket;
import java.util.ArrayList;

public class MediaReceiver {
    private Socket s;
    private String imageDir = Utils.tempImage;
    private String videoDir = Utils.videoServiceDir;
    private String audioDir = Utils.tempAudio;

    public MediaReceiver(Socket s) {
        this.s = s;
    }

    public String receiveImage() {
        String imagePath = this.receiveFrame();
        if (imagePath == null) {
            return null;
        }
        String finalPath = CmdLaunch.muxImage(imagePath, true);
        Utils.deleteFile(imagePath);

        return finalPath;
    }

    public String receiveFrame() {
        byte[] image = receiveChunk();
        if (image == null) {
            return null;
        }
        String imagePath = imageDir + Utils.randomId();
        Utils.saveBytes(image, imagePath);

        return imagePath;
    }

    public String receiveAudio() {
        byte[] audio = receiveChunk();

        String audioPath = audioDir + Utils.randomId();
        Utils.saveBytes(audio, audioPath);

        String finalPath = CmdLaunch.muxAudio(audioPath, true);
        Utils.deleteFile(audioPath);

        return finalPath;
    }

    public String receiveAudioAndCut() {
        byte[] audio = receiveChunk();

        try {
            DataInputStream inputStream = new DataInputStream(s.getInputStream());
            int start = inputStream.readInt();
            System.out.println("inizio a taglaire da: " + start);
            String audioPath = audioDir + Utils.randomId();
            Utils.saveBytes(audio, audioPath);

            String finalPath = CmdLaunch.muxAudioAndCut(audioPath, String.valueOf(start), true);
            Utils.deleteFile(audioPath);

            return finalPath;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public String receiveVideo() {
        byte[] video = receiveBytes();

        String videoPath = videoDir + Utils.randomId();
        Utils.saveBytes(video, videoPath);

        String finalPath = CmdLaunch.muxVideo(videoPath, true);
        Utils.deleteFile(videoPath);

        return finalPath;

    }

    private byte[] receiveChunk() {
        try {
            DataInputStream inputStream = new DataInputStream(s.getInputStream());
            int length = inputStream.readInt();
            if(length == -1){
                return null;
            }
            System.out.println(length);

            byte[] bytes = new byte[length];
            inputStream.readFully(bytes, 0, bytes.length);

            return bytes;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private byte[] receiveBytes() {
        int count = 0;
        ArrayList<Byte> video = new ArrayList<>(100000); // contiene tutti i byte del video
        try {
            DataInputStream inputStream = new DataInputStream(s.getInputStream());
            while (true) {
                int length = -1;
                try {
                    length = inputStream.readInt();
                } catch (Exception e) {
                    break;
                }
                System.out.println(length);

                byte[] chunk = new byte[length];
                inputStream.readFully(chunk, 0, chunk.length);

                for (int j = 0; j < length; j++) {
                    video.add(chunk[j]);
                }

                count++;
            }
            System.out.println("VIDEO RICEVUTO");
            byte[] toSave = new byte[video.size()];

            for (int k = 0; k < video.size(); k++) {
                toSave[k] = video.get(k);  // copio l'arrayList in un array di byte così da salvarli sul file system.
            }

            return toSave;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public String getImageDir() {
        return imageDir;
    }

    public void setImageDir(String imageDir) {
        this.imageDir = imageDir;
    }

    public String getVideoDir() {
        return videoDir;
    }

    public void setVideoDir(String videoDir) {
        this.videoDir = videoDir;
    }

    public String getAudioDir() {
        return audioDir;
    }

    public void setAudioDir(String audioDir) {
        this.audioDir = audioDir;
    }
}
