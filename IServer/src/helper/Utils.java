package helper;

import java.io.*;
import java.util.ArrayList;
import java.util.UUID;

public class Utils {
    public static final String clientDir = "data\\user_data\\client\\";
    public static final String tempImage = "data\\user_data\\temp_image\\";
    public static final String tempAudio = "data\\user_data\\temp_audio\\";
    public static final String servicesDir = "data\\user_data\\services\\";
    public static final String photoServiceDir = "data\\user_data\\services\\photos\\";
    public static final String videoServiceDir = "data\\user_data\\services\\video\\";
    public static final String emotionImagesDir = "data\\user_data\\services\\emotion_images\\";
    public static final String pythonScript = "data\\python\\FaceRecognition\\FaceRecognitionOpt2.py";
    public static final String idPath = "data\\user_data\\last_id.txt";

    public static void saveBytes(byte[] bytes,String filePath){

        File file = new File(filePath);
        OutputStream output = null;
        try {
            output = new FileOutputStream(file);
            output.write(bytes);
            output.flush();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (output != null) {
                try {
                    output.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static void deleteFile(String filePath){
        File file = new File(filePath);
        if(file.exists()) {
            file.delete();
        }else {
            System.out.println("file non trovato: "+ filePath);
        }
    }

    public static String randomId(){
        return  UUID.randomUUID().toString();
    }

    public static ArrayList<String> getDirectoryList(){
        ArrayList<String> directoryList = new ArrayList<>();
        directoryList.add(clientDir);
        directoryList.add(tempImage);
        directoryList.add(tempAudio);
        directoryList.add(servicesDir);
        directoryList.add(photoServiceDir);
        directoryList.add(videoServiceDir);
        directoryList.add(emotionImagesDir);

        return directoryList;
    }
}
