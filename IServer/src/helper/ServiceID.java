package helper;

public class ServiceID {
    public static final int LOGIN = 10;
    public static final int CONVERSATION = 20;
    public static final int PHOTO = 30;
    public static final int VIDEO = 40;
    public static final int IMPROVE_FACE_RECOGNITION = 50;
    public static final int SUCCESS = 200;
    public static final int FAILURE = -1;
    public static final int EMOTION_DETECTION = 60;

}
