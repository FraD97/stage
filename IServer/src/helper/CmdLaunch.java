package helper;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

public class CmdLaunch {

    public static String muxImage(String imagePath, boolean wait){
        String command = "ffmpeg -i "+ imagePath +" " + imagePath + ".jpg";
        launchCommand(command,wait);
        return imagePath+".jpg";
    }

    public static String muxVideo(String videoPath, boolean wait){
        String command = "ffmpeg -framerate 10 -i " + videoPath + " -r 24 -filter:v "+'"'+"setpts=0.5*PTS"+'"' +" "+videoPath + ".mp4";
        launchCommand(command,wait);
        return videoPath+".jpg";
    }

    public static String muxAudio(String audioPath,boolean wait){
        String command = "ffmpeg -i " + audioPath + " " + audioPath + ".wav";
        launchCommand(command,wait);
        return audioPath+".wav";
    }

    public static String muxAudioAndCut(String audioPath,String start ,boolean wait){
        String command = "ffmpeg -ss "+start+" -i " + audioPath + " " + audioPath + ".wav";
        launchCommand(command,wait);
        return audioPath+".wav";
    }

    public static int startFaceRecognition(String imagePath, String clientDir){
        int exitValue = -1;
        // imagePath è il path dell'immagine da confrontare, clientDir è la cartella del client corrente.
        System.out.println("lancio l'algoritmo python sulla directory -> " + clientDir);
        /*
        int exitValue = launchCommand("python " + Utils.pythonScript + " " + imagePath + " " + clientDir,true);
        return exitValue;
        */
        try {
            Socket socket = new Socket("127.0.0.1",8686);
            DataOutputStream dataOutputStream = new DataOutputStream(socket.getOutputStream());
            dataOutputStream.writeUTF(imagePath + "\n" + clientDir);

            DataInputStream input = new DataInputStream(socket.getInputStream());
            String fromPython = input.readUTF();
            exitValue = Integer.parseInt(fromPython);
            System.out.println("il python mi ha ritornato: " + fromPython);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return exitValue;
    }

    private static int launchCommand(String command,boolean wait){
        Process child = null;
        try {
            child = Runtime.getRuntime().exec(command);
            if(wait) {
                child.waitFor();
            }
            return child.exitValue();
        } catch (IOException e) {
            e.printStackTrace();
            return -1;
        } catch (InterruptedException e) {
            e.printStackTrace();
            return -1;
        }
    }
}
