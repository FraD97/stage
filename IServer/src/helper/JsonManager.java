package helper;

import org.json.JSONException;
import org.json.JSONObject;
import java.io.DataInputStream;
import java.io.IOException;
import java.net.Socket;

public class JsonManager {

    public static JSONObject getJsonFromClient(Socket s){
        try {
            DataInputStream inputStream = new DataInputStream(s.getInputStream());
            JSONObject jsonObject = new JSONObject(inputStream.readUTF());
            return jsonObject;
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return null;
    }

    public static JSONObject getJsonFromAffectiva(Socket s){
        try {
            DataInputStream inputStream = new DataInputStream(s.getInputStream());
            byte[] jsonByte = new byte[1000];
            inputStream.read(jsonByte);
            String jsonString = new String(jsonByte);
            System.out.println(jsonString);
            JSONObject jsonObject = new JSONObject(jsonString);

            return jsonObject;
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return null;
    }
}
