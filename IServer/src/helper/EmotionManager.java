package helper;

import model.FrameData;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Field;
import java.util.*;
import java.util.function.BiConsumer;

public class EmotionManager {
    private HashMap<String, EmotionValues> totalEmotions;

    private HashMap<String, Integer> ageData;
    private HashMap<String, Integer> ethnicityData;
    private HashMap<String, Integer> genderData;

    private JSONObject esitmatedData;

    private int numberOfFrames = 0;

    public EmotionManager() {
        totalEmotions = new HashMap<>();
        totalEmotions.put("joy", new EmotionValues(0, 0));
        totalEmotions.put("sadness", new EmotionValues(0, 0));
        totalEmotions.put("fear", new EmotionValues(0, 0));
        totalEmotions.put("surprise", new EmotionValues(0, 0));
        totalEmotions.put("disgust", new EmotionValues(0, 0));
        totalEmotions.put("contempt", new EmotionValues(0, 0));
        totalEmotions.put("anger", new EmotionValues(0, 0));
        ageData = new HashMap<>();
        ethnicityData = new HashMap<>();
        genderData = new HashMap<>();
    }

    public void updateValues(FrameData frameData) {
        updateValuesEmotions(frameData);
        updateValuesBiometricData(frameData);
        if (frameData.faceDetected()) {
            numberOfFrames++;
        }
    }

    private void updateValuesEmotions(FrameData frameData) {
        HashMap<String, Integer> emotions = frameData.getEmotions();
        Set set = emotions.keySet();
        for (Object o : set) {
            int value = emotions.get(o);
            if (value > 0) {
                totalEmotions.get(o).incrementOccurrences();
                totalEmotions.get(o).incrementValue(value);
            }
        }
    }

    private void updateValuesBiometricData(FrameData frameData) {
        HashMap<String, String> appearanceData = frameData.getAppearanceData();
        Set set = appearanceData.keySet();
        for (Object o : set) {
            if (appearanceData.get(o) != null && !appearanceData.get(o).equals("")) {
                switch ((String) o) {
                    case "age":
                        incrementBiometricOccurrences(ageData, appearanceData.get(o));
                        break;
                    case "gender":
                        incrementBiometricOccurrences(genderData, appearanceData.get(o));
                        break;
                    case "ethnicity":
                        incrementBiometricOccurrences(ethnicityData, appearanceData.get(o));
                        break;
                }
            }

        }
    }

    public String toString() {
        return totalEmotions.toString() + "\n" + ageData.toString() + "\n" + ethnicityData.toString() +
                "\n" + genderData.toString() + "\n" +
                "Frame numero: " + numberOfFrames;
    }

    private void incrementBiometricOccurrences(HashMap<String, Integer> hashMap, String key) {
        if (hashMap.get(key) == null) {
            hashMap.put(key, 1);
        } else {
            hashMap.replace(key, hashMap.get(key) + 1);
        }
    }

    public JSONObject createEstimatedData() {
        esitmatedData = new JSONObject();
        calculateAge();
        calculateGender();
        calculateEthnicity();
        calculateEmotion();

        return esitmatedData;
    }


    private void calculateAge() {
        Set set = ageData.keySet();
        int maxOccurrences = 0;
        String age = null;
        for (Object o : set) {
            System.out.println("Risultato: " + ageData.get(o));
            if (ageData.get(o) > maxOccurrences) {
                maxOccurrences = ageData.get(o);
                age = (String) o;
            }
        }
        try {
            esitmatedData.put("age", age);
            if (numberOfFrames != 0) {

                esitmatedData.put("probabilityAge", (int) (((double) maxOccurrences / (double) numberOfFrames) * 100));
            } else {
                esitmatedData.put("probabilityAge", 0);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        System.out.println(numberOfFrames);
        System.out.println(esitmatedData);
    }

    private void calculateGender() {
        if (genderData.get("Male") == null) {
            genderData.put("Male", 0);
        }
        if (genderData.get("Female") == null) {
            genderData.put("Female", 0);
        }
        if (genderData.get("Male") > genderData.get("Female")) {
            try {
                esitmatedData.put("gender", "Male");

            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else if (genderData.get("Male") < genderData.get("Female")) {
            try {
                esitmatedData.put("gender", "Male");

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        else{
            try {
                esitmatedData.put("gender", "Unknown");

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private void calculateEthnicity() {
        Set set = ethnicityData.keySet();
        int maxOccurrences = 0;
        String ethnicity = null;
        for (Object o : set) {
            if (ethnicityData.get(o) > maxOccurrences) {
                maxOccurrences = ethnicityData.get(o);
                ethnicity = (String) o;
            }
        }
        try {
            esitmatedData.put("ethnicity", ethnicity);
            if (maxOccurrences != 0) {
                esitmatedData.put("probabilityEthnicity", (int) (((double) maxOccurrences / (double) numberOfFrames) * 100));
            } else {
                esitmatedData.put("probabilityEthnicity", 0);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void calculateEmotion() {
        ArrayList<Map.Entry<String, EmotionValues>> emotionsList = new ArrayList<>(totalEmotions.entrySet());
        emotionsList.sort(Map.Entry.comparingByValue(new EmotionValuesComparator()));
        String firstEmotion = emotionsList.get(0).getKey();
        String secondEmotion = emotionsList.get(1).getKey();
        String thirdEmotion = emotionsList.get(2).getKey();
        try {
            esitmatedData.put("firstEmotion", firstEmotion);
            esitmatedData.put("secondEmotion", secondEmotion);
            esitmatedData.put("thirdEmotion", thirdEmotion);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private class EmotionValues {
        private int occurrences;
        private int value;

        public EmotionValues(int occurrences, int value) {
            this.occurrences = occurrences;
            this.value = value;
        }

        public void incrementOccurrences() {
            occurrences++;
        }

        public void incrementValue(int value) {
            this.value += value;
        }

        public float getAverage() {
            if (this.occurrences != 0) {
                return this.value / this.occurrences;
            }
            return 0;
        }

        public int getOccurrences() {
            return occurrences;
        }

        public void setOccurrences(int occurrences) {
            this.occurrences = occurrences;
        }

        public int getValue() {
            return value;
        }

        public void setValue(int value) {
            this.value = value;
        }

        @Override
        public String toString() {
            return "EmotionValues{" +
                    "occurrences=" + occurrences +
                    ", value=" + value +
                    '}';
        }
    }

    private class EmotionValuesComparator implements Comparator<EmotionValues> {

        @Override
        public int compare(EmotionValues o1, EmotionValues o2) {
            if (o1.getAverage() < o2.getAverage()) {
                return 1;
            } else if (o1.getAverage() > o2.getAverage()) {
                return -1;
            }
            return 0;
        }
    }
}
