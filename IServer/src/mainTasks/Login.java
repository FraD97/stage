package mainTasks;

import database.DatabaseHelper;
import helper.Utils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.net.Socket;

import static helper.ServiceID.SUCCESS;

public class Login implements Runnable {
    private Socket s;
    private String clientName;

    public Login(Socket s, String clientName) {
        this.s = s;
        this.clientName = clientName;
    }

    @Override
    public void run() {
        startLogin();
    }

    public void startLogin() {
        DatabaseHelper databaseHelper = new DatabaseHelper();
        databaseHelper.connect();
        int clientID = -1;
        System.out.println("login effettuato: " + clientName);
        if (!checkClientExistence()) {
            clientID = databaseHelper.insertClient(clientName);
        } else {
            clientID = databaseHelper.getUserIdFromName(clientName);
        }
        confirmToClient(clientID);
    }

    private void confirmToClient(int clientID) {
        JSONObject response = new JSONObject();
        try {
            DataOutputStream dataOutputStream = new DataOutputStream(s.getOutputStream());
            response.put("responseCode", SUCCESS);
            response.put("client", clientName);
            response.put("userID", clientID);
            dataOutputStream.writeUTF(response.toString());
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private boolean checkClientExistence() {
        File client = new File(Utils.clientDir + "\\" + clientName + "\\");
        if (client.exists()) {
            System.out.println("il client " + clientName + " esiste già");
            return true;
        } else {
            client.mkdir();
            System.out.println("creata la directory -> " + client.getAbsolutePath());
            return false;
        }
    }
}
