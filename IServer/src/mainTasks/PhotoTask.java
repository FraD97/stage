package mainTasks;

import helper.MediaReceiver;
import helper.Utils;

import java.io.IOException;
import java.net.Socket;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.UUID;

public class PhotoTask implements  Runnable{
    private Socket s;

    public PhotoTask(Socket s) {
        this.s = s;
    }

    @Override
    public void run() {
        try {
            receivePhoto();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void receivePhoto() throws IOException {
        MediaReceiver mediaReceiver = new MediaReceiver(s);
        String imageReceived = mediaReceiver.receiveImage();
        Files.move(Paths.get(imageReceived),Paths.get(Utils.photoServiceDir + UUID.randomUUID()+".jpg"));
    }
}
