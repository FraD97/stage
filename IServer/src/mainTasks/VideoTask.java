package mainTasks;

import helper.MediaReceiver;
import helper.Utils;

import java.io.IOException;
import java.net.Socket;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.UUID;

public class VideoTask implements Runnable{
    private Socket s;

    public VideoTask(Socket s) {
        this.s = s;
    }

    @Override
    public void run() {
        System.out.println("INIZIO IL VIDEO TASK");
        try {
            receiveVideo();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public  void receiveVideo() throws IOException {
        MediaReceiver mediaReceiver = new MediaReceiver(s);
        String videoReceived = mediaReceiver.receiveVideo();
        Files.move(Paths.get(videoReceived), Paths.get(Utils.videoServiceDir + UUID.randomUUID()+".mp4"));
    }
}
