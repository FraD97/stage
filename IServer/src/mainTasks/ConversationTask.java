package mainTasks;

import org.json.JSONException;
import org.json.JSONObject;
import subTasks.Dialogue;
import subTasks.FaceRecognizer;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

import static helper.ServiceID.FAILURE;
import static helper.ServiceID.SUCCESS;

public class ConversationTask implements  Runnable{
    private Socket s;
    private String clientName;

    public ConversationTask(Socket s,String clientName){
        this.s = s;
        this.clientName = clientName;
    }

    public ConversationTask(){

    }

    @Override
    public void run() {
        System.out.println("conversazione iniziata col client " + clientName);
        int userID = this.recognizeFace();
        System.out.println("UserID restituito dal python: " + userID);
        while(userID == -1 || userID == -2){ //-1 volto non rilevato, -2 rilevati 2 o più volti nella foto.
            sendResponseCode(FAILURE,userID);
            userID = this.recognizeFace();
        }
        startDialogue(userID);
    }

    public void startDialogue(int userID){
        Dialogue dialogue = new Dialogue(s,userID);
        dialogue.startDialogue();
        try {
            s.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public int recognizeFace(){
        FaceRecognizer faceRecognizer = new FaceRecognizer(s,clientName);
        int userID = faceRecognizer.startRecognition();

        return userID;
    }

    private void sendResponseCode(int responseCode,int userID){
        JSONObject response = new JSONObject();
        try {
            DataOutputStream dataOutputStream = new DataOutputStream(s.getOutputStream());
            response.put("responseCode",responseCode);
            if(responseCode == SUCCESS) {
                response.put("question", "");
            }else{
                if(userID == -1) {
                    response.put("question", "Scusami, non ho rilevato un volto, premi scatta foto per rifarla.");
                }else if(userID == -2){
                    response.put("question", "Scusami, non riesco a gestire due persone, provate uno per volta e premi scatta foto per rifarla.");
                }
            }
            response.put("response","");
            dataOutputStream.writeUTF(response.toString());
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
