package mainTasks;

import helper.EmotionManager;
import helper.JsonManager;
import helper.MediaReceiver;
import helper.Utils;
import model.FrameData;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.net.Socket;


public class EmotionTask implements Runnable {
    private Socket socket;
    private DataOutputStream outputStreamEmotion;
    private DataInputStream inputStream;
    private DataOutputStream outputStream;
    private EmotionManager emotionManager;

    public EmotionTask(Socket socket) {
        this.socket = socket;
        emotionManager = new EmotionManager();
    }

    @Override
    public void run() {
        try {
            detectEmotion();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void detectEmotion() throws IOException {
        MediaReceiver mediaReceiver = new MediaReceiver(socket);
        mediaReceiver.setImageDir(Utils.emotionImagesDir);
        String imagePath = "";
        Socket emotionSocket = new Socket("localhost", 4444);
        sendRequestEmotion(1, emotionSocket);
        while (true) {
            imagePath = mediaReceiver.receiveImage();
            if (imagePath == null) {
                break;
            }
            sendPath(imagePath, emotionSocket);
            FrameData frameData = getFrameDataFromAffectiva(emotionSocket);
            Utils.deleteFile(imagePath);
            frameData.findDominantEmotion();
            emotionManager.updateValues(frameData);
            sendFrameDataToClient(frameData);
        }
        sendEstimatedDataToClient();
        emotionSocket.close();
    }

    public void sendPath(String imagePath, Socket emotionSocket) {
        try {
            if (imagePath != null) {
                outputStreamEmotion = new DataOutputStream(emotionSocket.getOutputStream());
                outputStreamEmotion.writeUTF(imagePath);

            }
        } catch (IOException e) {
            e.printStackTrace();

        }
    }

    private void sendEstimatedDataToClient() {
        JSONObject jsonData = emotionManager.createEstimatedData();
        try {
            jsonData.put("last", "true");
            outputStream.writeUTF(jsonData.toString());
            System.out.println("Valore finale jsondata: " + jsonData);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void sendFrameDataToClient(FrameData frameData) {
        if (outputStream == null) {
            try {
                outputStream = new DataOutputStream(socket.getOutputStream());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        try {
            JSONObject jsonObject = frameData.getJsonObject();
            jsonObject.put("last", "false");
            outputStream.writeUTF(jsonObject.toString());
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void sendRequestEmotion(int service, Socket socket) {
        try {
            outputStreamEmotion = new DataOutputStream(socket.getOutputStream());
            outputStreamEmotion.write(service);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private FrameData getFrameDataFromAffectiva(Socket emotionSocket) {
        JSONObject jsonObject = JsonManager.getJsonFromAffectiva(emotionSocket);
        return new FrameData(jsonObject);
    }

}