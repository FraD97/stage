package database;

import model.Attribute;
import model.Person;
import model.Question;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class DatabaseHelper {

    private Connection conn;

    public DatabaseHelper() {
    }

    public void connect() {
        try {
            conn = DriverManager.getConnection(
                    "jdbc:mysql://localhost:3306/sanbot_db?verifyServerCertificate=false&useSSL=true",
                    "root", "");

        } catch (SQLException ex) {
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
        }
        System.out.println("Connesso al server!");
    }

    public Question getQuestion(int id) {
        Question question = null;
        try {
            String query = "SELECT * FROM domanda WHERE id = " + id;
            Statement statement = conn.createStatement();
            ResultSet resultSet = statement.executeQuery(query);

            while (resultSet.next()) {
                //id, attributo, testo, conf, tono
                int idQ = resultSet.getInt("id");
                int attribute = resultSet.getInt("attributo");
                String text = resultSet.getString("testo");
                int conf = resultSet.getInt("confidenzialità");
                int tono = resultSet.getInt("tono");

                question = new Question(idQ, attribute, text, conf, tono);

            }
            System.out.println("Query eeguita");
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return question;
    }

    public void insertPerson(int userID) {
        String query = "INSERT INTO persona(id) VALUES(?)";
        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = conn.prepareStatement(query);
            preparedStatement.setInt(1, userID);
            preparedStatement.execute();

            insertAppartenenzaAP(userID);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public Person getPerson(int userID) {
        String query = "SELECT * FROM appartenenza_pa where persona = " + userID;
        Person person = null;
        try {
            Statement statement = conn.createStatement();
            ResultSet resultSet = statement.executeQuery(query);
            List<Attribute> attributes = new ArrayList<>();
            while (resultSet.next()) {
                int attributeID = resultSet.getInt("attributo");
                String value = resultSet.getString("valore");
                Attribute attribute = new Attribute(attributeID, value);
                attributes.add(attribute);
            }
            person = new Person(userID, attributes);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return person;
    }

    public int insertClient(String clientName){
        String query = "INSERT INTO client(username) VALUES(?)";
        PreparedStatement preparedStatement = null;
        int id = -1;
        try {
            preparedStatement = conn.prepareStatement(query,Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setString(1, clientName);
            preparedStatement.execute();

            ResultSet rs = preparedStatement.getGeneratedKeys();
            if (rs.next()) {
                id = rs.getInt(1);
                System.out.println("id del client appena aggiunto: " + id);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return id;
    }

    public int getUserIdFromName(String clientName){
        String query = "SELECT id FROM client where username like " + "'" + clientName +"'";
        int id = -1;
        try {
            Statement statement = conn.createStatement();
            ResultSet resultSet = statement.executeQuery(query);
            while (resultSet.next()) {
                id = resultSet.getInt("id");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return id;
    }

    public List<Question> getUnfilledQuestions(List<Attribute> attributes) {
        List<Attribute> subList = new ArrayList<>();

        for(Attribute attribute : attributes){
            if(attribute.getValue() == null){
                subList.add(attribute);
            }
        }

        List<Question> questions = new ArrayList<>();
        if(!subList.isEmpty()) {
            StringBuilder stringSet = new StringBuilder("(");
            for (Attribute attribute : subList) {
                stringSet.append(attribute.getId() + ",");
            }
            stringSet.deleteCharAt(stringSet.length() - 1);
            stringSet.append(")");
            String query = "SELECT * FROM domanda WHERE attributo in " + stringSet;
            System.out.println(query);
            Statement statement = null;
            try {
                statement = conn.createStatement();
                ResultSet resultSet = statement.executeQuery(query);
                while(resultSet.next()){
                    int id = resultSet.getInt("id");
                    int attribute = resultSet.getInt("attributo");
                    String text = resultSet.getString("testo");
                    int confidentiality = resultSet.getInt("confidenzialità");
                    int tone = resultSet.getInt("tono");
                    Question question = new Question(id, attribute, text, confidentiality, tone);
                    questions.add(question);
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return questions;
    }

    public void registerAttributeValue(int id, int attribute, String value){
        String query = "UPDATE appartenenza_pa SET valore = ? WHERE attributo = ? AND persona = ?";
        try {
            PreparedStatement preparedStatement = conn.prepareStatement(query);
            preparedStatement.setString(1,value);
            preparedStatement.setInt(2,attribute);
            preparedStatement.setInt(3,id);
            preparedStatement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private List<Integer> getAllAttributesID() {
        String query = "SELECT id FROM attributo";
        Statement statement = null;
        List<Integer> idList = new ArrayList<>();
        try {
            statement = conn.createStatement();
            ResultSet rs = statement.executeQuery(query);

            while (rs.next()) {
                int id = rs.getInt("id");
                idList.add(id);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return idList;
    }

    private void insertAppartenenzaAP(int userID) {
        List<Integer> attributes = getAllAttributesID();
        String query = "INSERT INTO appartenenza_pa(persona,attributo) VALUES(?,?)";
        try {
            PreparedStatement preparedStatement = conn.prepareStatement(query);
            for (Integer id : attributes) {
                preparedStatement.setInt(1, userID);
                preparedStatement.setInt(2, id);
                preparedStatement.addBatch();
            }
            preparedStatement.executeBatch();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
